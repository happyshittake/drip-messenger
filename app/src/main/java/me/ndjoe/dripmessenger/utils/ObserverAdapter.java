package me.ndjoe.dripmessenger.utils;

import rx.Observer;

public class ObserverAdapter<T> implements Observer<T> {

  private static final ObserverAdapter<Object> EMPTY = new ObserverAdapter<>();

  @SuppressWarnings("unchecked") public static <R> ObserverAdapter<R> empty() {
    return (ObserverAdapter<R>) EMPTY;
  }

  @Override public void onCompleted() {

  }

  @Override public void onError(Throwable e) {

  }

  @Override public void onNext(T t) {

  }
}
