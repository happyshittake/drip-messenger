package me.ndjoe.dripmessenger.utils;

import android.text.format.DateUtils;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class DateFormatter {
  public static final SimpleDateFormat todayDateFormat =
      new SimpleDateFormat("HH:mm", Locale.getDefault());
  public static final SimpleDateFormat otherDateFormat =
      new SimpleDateFormat("dd.MM", Locale.getDefault());

  public static String messageDate(Date date) {
    return DateUtils.isToday(date.getTime()) ? DateFormatter.todayDateFormat.format(date)
        : DateFormatter.otherDateFormat.format(date);
  }
}
