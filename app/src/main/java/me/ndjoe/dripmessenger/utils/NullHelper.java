package me.ndjoe.dripmessenger.utils;

public class NullHelper {
  public static String nonNull(String s) {
    if (s == null) {
      return "";
    }
    return s;
  }
}
