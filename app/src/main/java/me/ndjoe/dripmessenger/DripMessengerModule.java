package me.ndjoe.dripmessenger;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import dagger.Module;
import dagger.Provides;
import javax.inject.Singleton;
import me.ndjoe.dripmessenger.data.XMPPConfig;
import org.greenrobot.eventbus.EventBus;

@Module public class DripMessengerModule {
  private DripMessenger app;

  public DripMessengerModule(DripMessenger dripMessenger) {
    this.app = dripMessenger;
  }

  @Provides @Singleton Context provideContext() {
    return app.getApplicationContext();
  }

  @Provides @Singleton SharedPreferences providePreferences() {
    return PreferenceManager.getDefaultSharedPreferences(app.getApplicationContext());
  }

  @Provides @Singleton EventBus provideEventBus() {
    return EventBus.getDefault();
  }

  @Provides @Singleton XMPPConfig provideXmppConfig() {
    return new XMPPConfig("128.199.220.162", 5222, "drip", "drip-android");
  }
}
