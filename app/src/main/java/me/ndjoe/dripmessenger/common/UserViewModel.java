package me.ndjoe.dripmessenger.common;

public class UserViewModel {
  private String name;

  public UserViewModel(String name) {
    this.name = name;
  }

  public String getName() {
    return name;
  }
}
