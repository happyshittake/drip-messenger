package me.ndjoe.dripmessenger.common;

import me.ndjoe.dripmessenger.utils.DateFormatter;

public class ChatViewModel {
  private String id;
  private String name;
  private int unreadMessageCount;
  private MessageViewModel lastMessage;
  private String type;

  public ChatViewModel(String id, String name, int unreadMessageCount) {
    this.id = id;
    this.name = name;
    this.unreadMessageCount = unreadMessageCount;
    this.type = "chat";
  }

  public void setId(String id) {
    this.id = id;
  }

  public void setName(String name) {
    this.name = name;
  }

  public void setUnreadMessageCount(int unreadMessageCount) {
    this.unreadMessageCount = unreadMessageCount;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public void setLastMessage(MessageViewModel message) {
    lastMessage = message;
  }

  public String getId() {
    return id;
  }

  public String getName() {
    return name;
  }

  public String getUnreadMessageCount() {
    return String.valueOf(unreadMessageCount);
  }

  public boolean isHasUnreadMessages() {
    return unreadMessageCount != 0;
  }

  public int getUnreadMessageCountRaw() {
    return unreadMessageCount;
  }

  public String getLastMessage() {
    if (lastMessage != null) {
      return lastMessage.getBody();
    }

    return "";
  }

  public String getLastMessageTime() {
    if (lastMessage != null) {
      return DateFormatter.messageDate(lastMessage.getModel().getDate());
    }

    return "";
  }

  public MessageViewModel getLastMessageRaw() {
    return lastMessage;
  }
}
