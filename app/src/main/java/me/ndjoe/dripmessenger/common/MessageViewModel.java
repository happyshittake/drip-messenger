package me.ndjoe.dripmessenger.common;

import android.text.TextUtils;
import java.util.Date;
import me.ndjoe.dripmessenger.data.local.message.model.MessageModel;
import me.ndjoe.dripmessenger.data.local.message.model.MessageState;
import me.ndjoe.dripmessenger.utils.DateFormatter;

public class MessageViewModel {
  private String id;
  private int state;
  private Date date;
  private String body;
  private UserViewModel user;
  private boolean self;
  private String dateTime;
  private MessageModel model;
  private int weight;

  public MessageViewModel(String id, int weight, int state, Date date, String body,
      UserViewModel user, boolean self, MessageModel model) {
    this.id = id;
    this.weight = weight;
    this.state = state;
    this.date = date;
    this.body = body;
    this.user = user;
    this.self = self;
    this.model = model;

    this.dateTime = DateFormatter.messageDate(date);
  }

  public String getId() {
    return id;
  }

  public String getBody() {
    return body;
  }

  public boolean isHasBody() {
    return !TextUtils.isEmpty(body);
  }

  public boolean isSelf() {
    return self;
  }

  public Date getDate() {
    return date;
  }

  public String getTime() {
    return dateTime;
  }

  public int getWeight() {
    return weight;
  }

  public boolean isError() {
    return state == MessageState.STATE_ERROR;
  }

  public MessageModel getModel() {
    return model;
  }

  public UserViewModel getUser() {
    return user;
  }

  public boolean isHasName() {
    return !TextUtils.isEmpty(user.getName());
  }

  public String getName() {
    return user.getName();
  }
}
