package me.ndjoe.dripmessenger.common;

public interface BasePresenter<V> {
  void attachView(V view);

  void detachView();
}
