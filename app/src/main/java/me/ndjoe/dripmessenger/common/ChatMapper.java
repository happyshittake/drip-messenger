package me.ndjoe.dripmessenger.common;

import android.support.annotation.NonNull;
import java.util.List;
import java.util.Objects;
import me.ndjoe.dripmessenger.data.local.chat.model.ChatModel;
import me.ndjoe.dripmessenger.data.local.message.model.MessageModel;
import solid.collections.SolidList;
import solid.stream.Stream;

import static solid.collectors.ToList.toList;

public class ChatMapper {

  @NonNull public static List<ChatViewModel> map(List<ChatModel> chatModels) {
    return Stream.stream(chatModels).map(ChatMapper::map).sort((left, right) -> {
      if (Objects.equals(left.getId(), right.getId())) {
        return 0;
      }

      if (left.getLastMessageRaw() != null && right.getLastMessageRaw() != null) {
        return Long.compare(left.getLastMessageRaw().getDate().getTime(),
            right.getLastMessageRaw().getDate().getTime());
      } else if (left.getLastMessageRaw() != null) {
        return 1;
      } else if (right.getLastMessageRaw() != null) {
        return -1;
      }

      return left.getName().compareTo(right.getName());
    }).collect(toList());
  }

  @NonNull public static ChatViewModel map(ChatModel chatModel) {
    ChatViewModel viewModel = new ChatViewModel(chatModel.getJid(),
        chatModel.getName() == null ? chatModel.getJid() : chatModel.getName(),
        chatModel.getUnreadMessage());

    MessageModel lastMessage = chatModel.getLastMessage();
    if (lastMessage != null) {
      viewModel.setLastMessage(MessageMapper.map(lastMessage, null));
    }
    viewModel.setType(chatModel.getType());

    return viewModel;
  }
}
