package me.ndjoe.dripmessenger.common;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import java.util.List;
import me.ndjoe.dripmessenger.data.local.message.model.MessageModel;
import solid.stream.Stream;

import static solid.collectors.ToList.toList;

public class MessageMapper {
  @NonNull public static List<MessageViewModel> map(List<MessageModel> messageModels,
      @Nullable String selfUserId) {
    return Stream.stream(messageModels)
        .map(m -> MessageMapper.map(m, selfUserId))
        .sort((left, right) -> {
          if (left.getId().equals(right.getId())) {
            return 0;
          }

          return Integer.compare(left.getWeight(), right.getWeight());
        })
        .reverse()
        .collect(toList());
  }

  @NonNull public static MessageViewModel map(MessageModel model, @Nullable String selfUserId) {
    return new MessageViewModel(model.getId(), model.getWeight(), model.getState(), model.getDate(),
        model.getBody(), new UserViewModel(model.getFromUser()),
        (selfUserId != null && selfUserId.equals(model.getFromUser())), model);
  }
}
