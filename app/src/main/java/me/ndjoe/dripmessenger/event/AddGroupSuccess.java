package me.ndjoe.dripmessenger.event;

public class AddGroupSuccess {
  private String name;

  public AddGroupSuccess(String name) {
    this.name = name;
  }

  public String getName() {
    return name;
  }
}
