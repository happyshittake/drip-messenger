package me.ndjoe.dripmessenger.event;

public class AddGroup {
  private String topic;

  public AddGroup(String topic) {
    this.topic = topic;
  }

  public String getTopic() {
    return topic;
  }
}
