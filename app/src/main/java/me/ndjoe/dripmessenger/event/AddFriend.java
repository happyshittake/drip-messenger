package me.ndjoe.dripmessenger.event;

public class AddFriend {
  private String userJid;

  public AddFriend(String userJid) {
    this.userJid = userJid;
  }

  public String getUserJid() {
    return userJid;
  }
}
