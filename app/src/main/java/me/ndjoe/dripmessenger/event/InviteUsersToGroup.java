package me.ndjoe.dripmessenger.event;

public class InviteUsersToGroup {
  private String room;
  private CharSequence[] users;

  public InviteUsersToGroup(String room, CharSequence[] users) {
    this.room = room;
    this.users = users;
  }

  public String getRoom() {
    return room;
  }

  public CharSequence[] getUsers() {
    return users;
  }
}
