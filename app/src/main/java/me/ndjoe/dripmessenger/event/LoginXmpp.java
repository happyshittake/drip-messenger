package me.ndjoe.dripmessenger.event;

public class LoginXmpp {
  private String username;
  private String password;

  public LoginXmpp(String username, String password) {
    this.username = username;
    this.password = password;
  }

  public String getUsername() {
    return username;
  }

  public String getPassword() {
    return password;
  }
}
