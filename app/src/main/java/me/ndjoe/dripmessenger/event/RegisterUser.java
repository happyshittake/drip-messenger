package me.ndjoe.dripmessenger.event;

public class RegisterUser {
  private String username;
  private String password;

  public RegisterUser(String username, String password) {
    this.username = username;
    this.password = password;
  }

  public String getUsername() {
    return username;
  }

  public String getPassword() {
    return password;
  }
}
