package me.ndjoe.dripmessenger.event;

import org.jivesoftware.smackx.vcardtemp.packet.VCard;

public class VCardLoaded {
  private VCard vCard;

  public VCardLoaded(VCard vCard) {
    this.vCard = vCard;
  }

  public VCard getvCard() {
    return vCard;
  }
}
