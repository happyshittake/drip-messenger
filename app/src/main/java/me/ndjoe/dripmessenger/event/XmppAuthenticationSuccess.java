package me.ndjoe.dripmessenger.event;

public class XmppAuthenticationSuccess {
  private String username;
  private String password;

  public XmppAuthenticationSuccess(String username, String password) {
    this.username = username;
    this.password = password;
  }

  public String getUsername() {
    return username;
  }

  public String getPassword() {
    return password;
  }
}
