package me.ndjoe.dripmessenger.event;

import java.util.List;
import me.ndjoe.dripmessenger.recycleradapter.model.Contact;

public class ContactLoaded {
  private List<Contact> contacts;

  public ContactLoaded(List<Contact> contacts) {
    this.contacts = contacts;
  }

  public List<Contact> getContacts() {
    return contacts;
  }
}
