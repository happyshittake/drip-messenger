package me.ndjoe.dripmessenger;

import android.content.Context;
import android.content.SharedPreferences;
import com.pushtorefresh.storio.sqlite.StorIOSQLite;
import dagger.Component;
import javax.inject.Singleton;
import me.ndjoe.dripmessenger.data.di.Internal;
import me.ndjoe.dripmessenger.data.di.module.DbModule;
import me.ndjoe.dripmessenger.data.di.module.RepoModule;
import me.ndjoe.dripmessenger.data.local.chat.ChatRepository;
import me.ndjoe.dripmessenger.data.local.chat.LocalChatRepository;
import me.ndjoe.dripmessenger.data.local.message.LocalMessageRepository;
import me.ndjoe.dripmessenger.data.local.message.MessagesRepository;
import me.ndjoe.dripmessenger.data.xmpp.XMPPService;
import me.ndjoe.dripmessenger.ui.chatlist.di.ChatlistComponent;
import me.ndjoe.dripmessenger.ui.chatlist.di.ChatlistModule;
import me.ndjoe.dripmessenger.ui.chatroom.di.ChatRoomComponent;
import me.ndjoe.dripmessenger.ui.chatroom.di.ChatRoomModule;
import me.ndjoe.dripmessenger.ui.contacts.di.ContactsComponent;
import me.ndjoe.dripmessenger.ui.contacts.di.ContactsModule;
import me.ndjoe.dripmessenger.ui.login.di.LoginUIComponent;
import me.ndjoe.dripmessenger.ui.login.di.LoginUIModule;
import me.ndjoe.dripmessenger.ui.main.di.MainComponent;
import me.ndjoe.dripmessenger.ui.main.di.MainModule;
import org.greenrobot.eventbus.EventBus;

/**
 * Created by ndjoe on 01/12/16.
 */
@Singleton @Component(modules = { DbModule.class, RepoModule.class, DripMessengerModule.class })
public interface DripMessengerComponent {
  void inject(XMPPService xmppService);

  void inject(DripMessenger dripMessenger);

  LoginUIComponent plus(LoginUIModule module);

  MainComponent plus(MainModule module);

  ChatlistComponent plus(ChatlistModule module);

  ContactsComponent plus(ContactsModule module);

  ChatRoomComponent plus(ChatRoomModule module);

  Context context();

  MessagesRepository messagesRepository();

  ChatRepository chatRepository();

  @Internal StorIOSQLite storIoSqlite();

  LocalChatRepository localchatRepository();

  LocalMessageRepository localMessageRepository();

  EventBus bus();

  SharedPreferences preference();
}
