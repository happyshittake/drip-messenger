package me.ndjoe.dripmessenger.data.local.chat.criteria;

import com.pushtorefresh.storio.sqlite.queries.Query;
import me.ndjoe.dripmessenger.data.local.common.QueryCriteria;
import me.ndjoe.dripmessenger.data.local.db.table.ChatTable;

/**
 * Created by ndjoe on 07/12/16.
 */

public class ChatByIdCriteria extends QueryCriteria {
  private String id;

  private ChatByIdCriteria(String id) {
    this.id = id;
  }

  public static ChatByIdCriteria create(String id) {
    return new ChatByIdCriteria(id);
  }

  @Override public Query filter() {
    return Query.builder()
        .table(ChatTable.TABLE)
        .where(ChatTable.COLUMN_JID + " = ?")
        .whereArgs(this.id)
        .build();
  }

  public String getId() {
    return id;
  }

  @Override public String toString() {
    return "ChatByIdCriteria{" +
        "id='" + id + '\'' +
        '}';
  }
}
