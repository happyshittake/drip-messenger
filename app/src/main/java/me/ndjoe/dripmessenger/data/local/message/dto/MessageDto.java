package me.ndjoe.dripmessenger.data.local.message.dto;

import com.pushtorefresh.storio.sqlite.annotations.StorIOSQLiteColumn;
import com.pushtorefresh.storio.sqlite.annotations.StorIOSQLiteType;
import me.ndjoe.dripmessenger.data.local.db.table.MessagesTable;

/**
 * Created by ndjoe on 07/12/16.
 */
@StorIOSQLiteType(table = MessagesTable.TABLE) public class MessageDto {
  @StorIOSQLiteColumn(name = MessagesTable.COLUMN_CHAT_ID) public String chatId;
  @StorIOSQLiteColumn(name = MessagesTable.COLUMN_ID, key = true) public String id;
  @StorIOSQLiteColumn(name = MessagesTable.COLUMN_BODY) public String body;
  @StorIOSQLiteColumn(name = MessagesTable.COLUMN_STATE) public int state;
  @StorIOSQLiteColumn(name = MessagesTable.COLUMN_FROM_USER) public String fromUser;
  @StorIOSQLiteColumn(name = MessagesTable.COLUMN_TO_USER) public String toUser;
  @StorIOSQLiteColumn(name = MessagesTable.COLUMN_DATE) public long dateTime;
  @StorIOSQLiteColumn(name = MessagesTable.COLUMN_WEIGHT, ignoreNull = true) public Integer weight;
  @StorIOSQLiteColumn(name = MessagesTable.COLUMN_OFFLINE) public Boolean offline;
}
