package me.ndjoe.dripmessenger.data.local.message.resolvers;

import java.util.Date;
import me.ndjoe.dripmessenger.data.local.message.dto.MessageDto;
import me.ndjoe.dripmessenger.data.local.message.model.MessageModel;

/**
 * Created by ndjoe on 12/12/16.
 */

public class MessageMapper {
  static MessageDto map(MessageModel model) {
    MessageDto dto = new MessageDto();
    dto.chatId = model.getChatId();
    dto.id = model.getId();
    dto.dateTime = model.getDate().getTime();
    dto.state = model.getState();
    dto.fromUser = model.getFromUser();
    dto.toUser = model.getToUser();
    dto.offline = model.isOffline();
    dto.body = model.getBody();

    return dto;
  }

  static MessageModel map(MessageDto dto) {
    MessageModel model =
        new MessageModel(dto.id, dto.weight, dto.state, new Date(dto.dateTime), dto.fromUser,
            dto.toUser, dto.chatId, dto.offline);
    model.setBody(dto.body);
    return model;
  }
}
