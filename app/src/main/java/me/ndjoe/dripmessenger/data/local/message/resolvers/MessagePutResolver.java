package me.ndjoe.dripmessenger.data.local.message.resolvers;

import android.support.annotation.NonNull;
import com.pushtorefresh.storio.sqlite.StorIOSQLite;
import com.pushtorefresh.storio.sqlite.operations.put.PutResolver;
import com.pushtorefresh.storio.sqlite.operations.put.PutResult;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import me.ndjoe.dripmessenger.data.local.db.table.MessagesTable;
import me.ndjoe.dripmessenger.data.local.message.dto.MessageDto;
import me.ndjoe.dripmessenger.data.local.message.model.MessageModel;

/**
 * Created by ndjoe on 13/12/16.
 */

public class MessagePutResolver extends PutResolver<MessageModel> {
  final Set<String> affectedTables = new HashSet<>(1);

  {
    affectedTables.add(MessagesTable.TABLE);
  }

  @NonNull @Override
  public PutResult performPut(@NonNull StorIOSQLite storIOSQLite, @NonNull MessageModel object) {
    ArrayList<Object> objects = new ArrayList<>();

    MessageDto dto = MessageMapper.map(object);
    objects.add(dto);

    storIOSQLite.put().objects(objects).prepare().executeAsBlocking();

    return PutResult.newUpdateResult(objects.size(), affectedTables);
  }
}
