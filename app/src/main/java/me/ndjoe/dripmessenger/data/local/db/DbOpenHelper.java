package me.ndjoe.dripmessenger.data.local.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import javax.inject.Inject;
import me.ndjoe.dripmessenger.data.local.db.table.ChatTable;
import me.ndjoe.dripmessenger.data.local.db.table.ImageTable;
import me.ndjoe.dripmessenger.data.local.db.table.MessagesTable;

public class DbOpenHelper extends SQLiteOpenHelper {
  public static final String DB_NAME = "dripdb2.db";
  private static final int VERSION = 1;

  @Inject public DbOpenHelper(Context context) {
    super(context, DB_NAME, null, VERSION);
  }

  @Override public void onCreate(SQLiteDatabase sqLiteDatabase) {
    sqLiteDatabase.execSQL(MessagesTable.getCreateStatement());
    sqLiteDatabase.execSQL(ChatTable.getCreateStatement());
    sqLiteDatabase.execSQL(ImageTable.getCreateStatement());
  }

  @Override public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

  }
}
