package me.ndjoe.dripmessenger.data.local.message;

import com.pushtorefresh.storio.sqlite.StorIOSQLite;
import java.util.List;
import javax.inject.Inject;
import me.ndjoe.dripmessenger.data.di.Internal;
import me.ndjoe.dripmessenger.data.local.common.Criteria;
import me.ndjoe.dripmessenger.data.local.common.QueryCriteria;
import me.ndjoe.dripmessenger.data.local.message.model.MessageModel;
import rx.Observable;
import rx.subjects.PublishSubject;

/**
 * Created by ndjoe on 07/12/16.
 */

public class LocalMessageRepository implements MessagesRepository {
  private StorIOSQLite db;

  private PublishSubject<MessageModel> changeNotificationSubject = PublishSubject.create();

  @Inject public LocalMessageRepository(@Internal StorIOSQLite db) {
    this.db = db;
  }

  @Override public Observable<List<MessageModel>> get(Criteria criteria) {
    QueryCriteria queryCriteria = (QueryCriteria) criteria;
    return db.get()
        .listOfObjects(MessageModel.class)
        .withQuery(queryCriteria.filter())
        .prepare()
        .asRxObservable()
        .take(1);
  }

  @Override public Observable<MessageModel> notifyUpdates(String chatId) {
    return changeNotificationSubject.filter(
        messageModel -> messageModel.getChatId().equals(chatId));
  }

  @Override public Observable<MessageModel> notifyUpdates() {
    return changeNotificationSubject;
  }

  @Override public Observable<MessageModel> put(MessageModel messageModel) {
    return db.put()
        .object(messageModel)
        .prepare()
        .asRxObservable()
        .flatMap(putResult -> Observable.just(messageModel))
        .doOnNext(messageModel1 -> changeNotificationSubject.onNext(messageModel));
  }
}
