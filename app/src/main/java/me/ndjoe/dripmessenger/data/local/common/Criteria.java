package me.ndjoe.dripmessenger.data.local.common;

/**
 * Created by ndjoe on 06/12/16.
 */

public interface Criteria<T> {
  T filter();
}
