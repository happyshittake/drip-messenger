package me.ndjoe.dripmessenger.data.local.message.criteria;

import com.pushtorefresh.storio.sqlite.queries.Query;
import java.util.ArrayList;
import me.ndjoe.dripmessenger.data.local.common.QueryCriteria;
import me.ndjoe.dripmessenger.data.local.db.table.MessagesTable;

/**
 * Created by ndjoe on 07/12/16.
 */

public class MessageStateCriteria extends QueryCriteria {
  private String chatId;
  private int[] states;

  public MessageStateCriteria(String chatId, int[] states) {
    this.chatId = chatId;
    this.states = states;
  }

  public static MessageStateCriteria create(String chatId, int... states) {
    return new MessageStateCriteria(chatId, states);
  }

  @Override public Query filter() {
    ArrayList<Object> whereArgs = new ArrayList<>();
    whereArgs.add(chatId);

    StringBuilder where = new StringBuilder(MessagesTable.COLUMN_CHAT_ID + " = ? AND (");
    for (int i = 0; i < states.length; i++) {
      if (i > 0) {
        where.append(" OR ");
      }

      where.append(MessagesTable.COLUMN_STATE + " =?");
      whereArgs.add(this.states[i]);
    }
    where.append(")");

    return Query.builder()
        .table(MessagesTable.TABLE)
        .where(where.toString())
        .whereArgs(whereArgs)
        .orderBy(MessagesTable.COLUMN_DATE + " DESC")
        .build();
  }
}
