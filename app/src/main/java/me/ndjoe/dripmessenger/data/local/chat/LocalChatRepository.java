package me.ndjoe.dripmessenger.data.local.chat;

import com.pushtorefresh.storio.sqlite.StorIOSQLite;
import com.pushtorefresh.storio.sqlite.queries.DeleteQuery;
import java.util.List;
import javax.inject.Inject;
import me.ndjoe.dripmessenger.data.di.Internal;
import me.ndjoe.dripmessenger.data.local.chat.criteria.ChatByIdCriteria;
import me.ndjoe.dripmessenger.data.local.chat.dto.ChatDto;
import me.ndjoe.dripmessenger.data.local.chat.model.ChatModel;
import me.ndjoe.dripmessenger.data.local.common.Criteria;
import me.ndjoe.dripmessenger.data.local.common.QueryCriteria;
import me.ndjoe.dripmessenger.data.local.db.table.ChatTable;
import rx.Observable;
import rx.Single;
import solid.stream.Stream;

import static solid.collectors.ToList.toList;

/**
 * Created by ndjoe on 07/12/16.
 */

public class LocalChatRepository implements ChatRepository {
  private StorIOSQLite storIOSQLite;

  @Inject public LocalChatRepository(@Internal StorIOSQLite storIOSQLite) {
    this.storIOSQLite = storIOSQLite;
  }

  @Override public Observable<ChatModel> get(Criteria criteria) {
    QueryCriteria queryCriteria = (QueryCriteria) criteria;
    return storIOSQLite.get()
        .listOfObjects(ChatDto.class)
        .withQuery(queryCriteria.filter())
        .prepare()
        .asRxObservable()
        .take(1)
        .flatMap(Observable::from)
        .map(ChatMapper::map);
  }

  @Override public Observable<ChatModel> put(ChatModel chatModel) {
    return Observable.just(chatModel).flatMap(model -> {
      ChatDto dto = ChatMapper.map(model);
      return storIOSQLite.put().object(dto).prepare().asRxObservable().map(putResult -> chatModel);
    });
  }

  @Override public Observable<ChatModel> remove(ChatModel chatModel) {
    return Observable.just(chatModel).flatMap(model -> {
      ChatDto dto = ChatMapper.map(model);
      return storIOSQLite.delete()
          .object(dto)
          .prepare()
          .asRxObservable()
          .map(putResult -> chatModel);
    });
  }

  @Override public Observable<Void> clear() {
    return storIOSQLite.delete()
        .byQuery(DeleteQuery.builder().table(ChatTable.TABLE).build())
        .prepare()
        .asRxObservable()
        .map(deleteResult -> null);
  }

  @Override public Observable<List<ChatDto>> getByIdRaw(String chatId) {
    return storIOSQLite.get()
        .listOfObjects(ChatDto.class)
        .withQuery(ChatByIdCriteria.create(chatId).filter())
        .prepare()
        .asRxObservable();
  }

  @Override public List<ChatModel> getChats(Criteria criteria) {
    QueryCriteria queryCriteria = (QueryCriteria) criteria;
    List<ChatDto> dtos = storIOSQLite.get()
        .listOfObjects(ChatDto.class)
        .withQuery(queryCriteria.filter())
        .prepare()
        .executeAsBlocking();
    return Stream.stream(dtos).map(ChatMapper::map).collect(toList());
  }
}
