package me.ndjoe.dripmessenger.data.local.message.resolvers;

import android.database.Cursor;
import android.support.annotation.NonNull;
import com.pushtorefresh.storio.sqlite.StorIOSQLite;
import com.pushtorefresh.storio.sqlite.operations.get.GetResolver;
import com.pushtorefresh.storio.sqlite.queries.Query;
import com.pushtorefresh.storio.sqlite.queries.RawQuery;
import me.ndjoe.dripmessenger.data.local.message.dto.MessageDto;
import me.ndjoe.dripmessenger.data.local.message.dto.MessageDtoStorIOSQLiteGetResolver;
import me.ndjoe.dripmessenger.data.local.message.model.MessageModel;

/**
 * Created by ndjoe on 13/12/16.
 */

public class MessageGetResolver extends GetResolver<MessageModel> {
  private final ThreadLocal<StorIOSQLite> storIOSQLiteFromPerformGet = new ThreadLocal<>();
  private final MessageDtoStorIOSQLiteGetResolver messageDtoStorIOSQLiteGetResolver;

  public MessageGetResolver() {
    this.messageDtoStorIOSQLiteGetResolver = new MessageDtoStorIOSQLiteGetResolver();
  }

  @NonNull @Override public MessageModel mapFromCursor(@NonNull Cursor cursor) {
    MessageDto messageDto = messageDtoStorIOSQLiteGetResolver.mapFromCursor(cursor);
    return MessageMapper.map(messageDto);
  }

  @NonNull @Override
  public Cursor performGet(@NonNull StorIOSQLite storIOSQLite, @NonNull RawQuery rawQuery) {
    storIOSQLiteFromPerformGet.set(storIOSQLite);
    return storIOSQLite.lowLevel().rawQuery(rawQuery);
  }

  @NonNull @Override
  public Cursor performGet(@NonNull StorIOSQLite storIOSQLite, @NonNull Query query) {
    storIOSQLiteFromPerformGet.set(storIOSQLite);
    return storIOSQLite.lowLevel().query(query);
  }
}
