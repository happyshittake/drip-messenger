package me.ndjoe.dripmessenger.data.local.message;

import java.util.List;
import me.ndjoe.dripmessenger.data.local.common.Criteria;
import me.ndjoe.dripmessenger.data.local.message.model.MessageModel;
import rx.Observable;

/**
 * Created by ndjoe on 13/12/16.
 */

public interface MessagesRepository {
  Observable<List<MessageModel>> get(Criteria criteria);

  Observable<MessageModel> notifyUpdates(String chatId);

  Observable<MessageModel> notifyUpdates();

  Observable<MessageModel> put(MessageModel messageModel);
}
