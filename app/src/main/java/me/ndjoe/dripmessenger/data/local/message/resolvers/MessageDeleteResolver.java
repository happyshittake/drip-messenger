package me.ndjoe.dripmessenger.data.local.message.resolvers;

import android.support.annotation.NonNull;
import com.pushtorefresh.storio.sqlite.StorIOSQLite;
import com.pushtorefresh.storio.sqlite.operations.delete.DeleteResolver;
import com.pushtorefresh.storio.sqlite.operations.delete.DeleteResult;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import me.ndjoe.dripmessenger.data.local.db.table.MessagesTable;
import me.ndjoe.dripmessenger.data.local.message.dto.MessageDto;
import me.ndjoe.dripmessenger.data.local.message.model.MessageModel;

/**
 * Created by ndjoe on 12/12/16.
 */

public class MessageDeleteResolver extends DeleteResolver<MessageModel> {
  final Set<String> affectedTables = new HashSet<>(1);

  {
    affectedTables.add(MessagesTable.TABLE);
  }

  @NonNull @Override public DeleteResult performDelete(@NonNull StorIOSQLite storIOSQLite,
      @NonNull MessageModel object) {
    ArrayList<Object> objects = new ArrayList<>();
    MessageDto dto = MessageMapper.map(object);
    objects.add(dto);

    storIOSQLite.delete().objects(objects).prepare().executeAsBlocking();
    return DeleteResult.newInstance(objects.size(), affectedTables);
  }
}
