package me.ndjoe.dripmessenger.data;

import android.os.Parcel;
import android.os.Parcelable;

public class XMPPConfig implements Parcelable {
  public static final Creator<XMPPConfig> CREATOR = new Creator<XMPPConfig>() {
    @Override public XMPPConfig createFromParcel(Parcel in) {
      return new XMPPConfig(in);
    }

    @Override public XMPPConfig[] newArray(int size) {
      return new XMPPConfig[size];
    }
  };
  private static final int DEFAULT_RECONNECTION_DELAY = 30;
  private static final int DEFAULT_SEND_MESSAGE_TIMEOUT = 5;
  private String host;
  private int port;
  private String domain;
  private String resource;
  private int reconnectionDelay = DEFAULT_RECONNECTION_DELAY;
  private int sendMessageTimeout = DEFAULT_SEND_MESSAGE_TIMEOUT;

  public XMPPConfig(String host, int port, String domain, String resource) {
    this.host = host;
    this.port = port;
    this.domain = domain;
    this.resource = resource;
  }

  protected XMPPConfig(Parcel in) {
    resource = in.readString();
    domain = in.readString();
    port = in.readInt();
    host = in.readString();
    reconnectionDelay = in.readInt();
    sendMessageTimeout = in.readInt();
  }

  @Override public String toString() {
    return "XMPPConfig{" +
        "host='" + host + '\'' +
        ", port=" + port +
        ", domain='" + domain + '\'' +
        ", resource='" + resource + '\'' +
        ", reconnectionDelay=" + reconnectionDelay +
        ", sendMessageTimeout=" + sendMessageTimeout +
        '}';
  }

  public String getHost() {
    return host;
  }

  public int getPort() {
    return port;
  }

  public String getDomain() {
    return domain;
  }

  public String getResource() {
    return resource;
  }

  @Override public int describeContents() {
    return 0;
  }

  public int getReconnectionDelay() {
    return reconnectionDelay;
  }

  public int getSendMessageTimeout() {
    return sendMessageTimeout;
  }

  @Override public void writeToParcel(Parcel parcel, int i) {
    parcel.writeString(resource);
    parcel.writeString(domain);
    parcel.writeInt(port);
    parcel.writeString(host);
    parcel.writeInt(reconnectionDelay);
    parcel.writeInt(sendMessageTimeout);
  }
}
