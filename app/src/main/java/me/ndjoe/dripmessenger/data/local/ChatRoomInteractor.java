package me.ndjoe.dripmessenger.data.local;

import android.support.annotation.NonNull;
import java.util.List;
import javax.inject.Inject;
import me.ndjoe.dripmessenger.R;
import me.ndjoe.dripmessenger.data.local.chat.ChatRepository;
import me.ndjoe.dripmessenger.data.local.chat.MessageMapper;
import me.ndjoe.dripmessenger.data.local.chat.criteria.ChatByIdCriteria;
import me.ndjoe.dripmessenger.data.local.chat.model.ChatModel;
import me.ndjoe.dripmessenger.data.local.message.MessagesRepository;
import me.ndjoe.dripmessenger.data.local.message.criteria.MessageStateCriteria;
import me.ndjoe.dripmessenger.data.local.message.criteria.MessagesOffsetCriteria;
import me.ndjoe.dripmessenger.data.local.message.model.MessageModel;
import me.ndjoe.dripmessenger.data.local.message.model.MessageState;
import rx.Observable;
import rx.Subscription;
import rx.functions.Func1;
import rx.subjects.BehaviorSubject;
import rx.subjects.PublishSubject;
import rx.subjects.SerializedSubject;
import rx.subjects.Subject;

public class ChatRoomInteractor {
  private ChatRepository chatRepository;
  private MessagesRepository messagesRepository;

  private PublishSubject<Object> notficationSubject = PublishSubject.create();
  private Subscription subscription;

  private Subject<ChatModel, ChatModel> initSubject =
      new SerializedSubject<>(BehaviorSubject.create());

  @Inject
  public ChatRoomInteractor(ChatRepository chatRepository, MessagesRepository messagesRepository) {
    this.chatRepository = chatRepository;
    this.messagesRepository = messagesRepository;
  }

  public Observable<ChatModel> initWithChatId(String chatId) {
    return chatRepository.get(ChatByIdCriteria.create(chatId))
        .take(1)
        .doOnNext(initSubject::onNext);
  }

  public Observable<List<MessageModel>> getMessages(String chatId, int offset, int quantity) {
    return messagesRepository.get(MessagesOffsetCriteria.create(chatId, offset, quantity))
        .flatMap(markAsRead())
        .delaySubscription(initSubject);
  }

  public Observable<List<MessageModel>> getNewMessages(String chatId) {
    return messagesRepository.get(
        MessageStateCriteria.create(chatId, MessageState.STATE_NEW, MessageState.STATE_ERROR,
            MessageState.STATE_OUTGOING, MessageState.STATE_SENDING, MessageState.STATE_PREPARE))
        .doOnSubscribe(() -> {
          if (subscription != null) {
            subscription.unsubscribe();
          }

          subscription = messagesRepository.notifyUpdates(chatId).filter(messageModel -> {
            int state = messageModel.getState();
            return (state == MessageState.STATE_NEW || state == MessageState.STATE_ERROR ||
                state == MessageState.STATE_OUTGOING || state == MessageState.STATE_SENDING ||
                state == MessageState.STATE_PREPARE);
          }).subscribe(messageModel -> notficationSubject.onNext(messageModel));
        })
        .flatMap(markAsRead())
        .repeatWhen(observable -> observable.flatMap(o -> notficationSubject.take(1)))
        .delaySubscription(initSubject);
  }

  public Observable<MessageModel> sendTextMessage(String text, String fromUser, String type) {
    return initSubject.take(1)
        .flatMap(chatModel -> messagesRepository.put(
            MessageMapper.map(text, chatModel, fromUser, type)));
  }

  public Observable<MessageModel> resendMessage(MessageModel model) {
    model.setState(MessageState.STATE_OUTGOING);
    return messagesRepository.put(model);
  }

  @NonNull
  private Func1<List<MessageModel>, ? extends Observable<? extends List<MessageModel>>> markAsRead() {
    return messageModels -> Observable.from(messageModels).flatMap(messageModel -> {
      if (messageModel.getState() == MessageState.STATE_NEW) {
        messageModel.setState(MessageState.STATE_READ);
        return messagesRepository.put(messageModel);
      }

      return Observable.just(messageModel);
    }).toList();
  }
}
