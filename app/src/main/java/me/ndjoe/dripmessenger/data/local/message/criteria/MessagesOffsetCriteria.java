package me.ndjoe.dripmessenger.data.local.message.criteria;

import com.pushtorefresh.storio.sqlite.queries.Query;
import me.ndjoe.dripmessenger.data.local.common.QueryCriteria;
import me.ndjoe.dripmessenger.data.local.db.table.MessagesTable;

/**
 * Created by ndjoe on 12/12/16.
 */

public class MessagesOffsetCriteria extends QueryCriteria {
  private String chatId;
  private int offset;
  private int quantity;

  public MessagesOffsetCriteria(String chatId, int offset, int quantity) {
    this.chatId = chatId;
    this.offset = offset;
    this.quantity = quantity;
  }

  public static MessagesOffsetCriteria create(String chatId, int offset, int quantity) {
    return new MessagesOffsetCriteria(chatId, offset, quantity);
  }

  @Override public Query filter() {
    return Query.builder()
        .table(MessagesTable.TABLE)
        .where(MessagesTable.COLUMN_CHAT_ID + " = ?")
        .whereArgs(chatId)
        .orderBy(MessagesTable.COLUMN_WEIGHT + " DESC")
        .limit(offset, quantity)
        .build();
  }
}
