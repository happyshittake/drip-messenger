package me.ndjoe.dripmessenger.data.xmpp;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import android.util.Log;
import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import me.ndjoe.dripmessenger.data.Logging;
import me.ndjoe.dripmessenger.data.XMPPConfig;
import me.ndjoe.dripmessenger.data.local.chat.model.ChatModel;
import me.ndjoe.dripmessenger.event.AddFriendFailed;
import me.ndjoe.dripmessenger.event.AddFriendSuccess;
import me.ndjoe.dripmessenger.event.AddGroupFailed;
import me.ndjoe.dripmessenger.event.AddGroupSuccess;
import me.ndjoe.dripmessenger.event.ContactLoaded;
import me.ndjoe.dripmessenger.event.VCardLoaded;
import me.ndjoe.dripmessenger.event.XmppAuthenticationError;
import me.ndjoe.dripmessenger.event.XmppAuthenticationSuccess;
import me.ndjoe.dripmessenger.recycleradapter.model.Contact;
import org.greenrobot.eventbus.EventBus;
import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.ConnectionListener;
import org.jivesoftware.smack.MessageListener;
import org.jivesoftware.smack.ReconnectionManager;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.chat.ChatManager;
import org.jivesoftware.smack.chat.ChatMessageListener;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Presence;
import org.jivesoftware.smack.roster.Roster;
import org.jivesoftware.smack.roster.RosterEntry;
import org.jivesoftware.smack.tcp.XMPPTCPConnection;
import org.jivesoftware.smack.tcp.XMPPTCPConnectionConfiguration;
import org.jivesoftware.smackx.iqregister.AccountManager;
import org.jivesoftware.smackx.muc.DiscussionHistory;
import org.jivesoftware.smackx.muc.MultiUserChat;
import org.jivesoftware.smackx.muc.MultiUserChatManager;
import org.jivesoftware.smackx.vcardtemp.VCardManager;
import org.jivesoftware.smackx.vcardtemp.packet.VCard;
import org.jivesoftware.smackx.xdata.Form;
import org.jivesoftware.smackx.xdata.packet.DataForm;
import rx.Emitter;
import rx.Observable;
import rx.subjects.BehaviorSubject;
import rx.subjects.PublishSubject;
import rx.subjects.SerializedSubject;
import rx.subjects.Subject;
import rx.subscriptions.Subscriptions;
import solid.collections.SolidSet;
import solid.functions.Action1;
import solid.stream.Stream;

import static solid.collectors.ToList.toList;

public class RxXMPP {
  public static final Integer CONNECTED = 0;
  public static final Integer DISCONNECTED = 1;
  private static final String TAG = "SMACK";
  private final XMPPConfig xmppConfig;

  private XMPPTCPConnection connection;
  private SharedPreferences preferences;
  private VCardManager vCardManager;

  private BehaviorSubject<Integer> authenticatedSubject = BehaviorSubject.create();
  private Observable<Integer> authenticatedNotification;

  private Subject<Message, Message> messageSubject =
      new SerializedSubject<>(PublishSubject.create());
  private ConnectionListener connectionListener = new ConnectionListener() {
    @Override public void connected(XMPPConnection connection) {
      Log.d(TAG, "connected");
      vCardManager = VCardManager.getInstanceFor(connection);
    }

    @Override public void authenticated(XMPPConnection connection, boolean resumed) {
      Log.d(TAG, "authenticated");
      if (Logging.ENABLED) {
      }

      authenticatedSubject.onNext(CONNECTED);
    }

    @Override public void connectionClosed() {
      if (Logging.ENABLED) {
        Log.d(TAG, "connection closed");
      }
    }

    @Override public void connectionClosedOnError(Exception e) {
      if (Logging.ENABLED) {
        Log.d(TAG, "connection closed on error: " + e);
      }
      authenticatedSubject.onNext(DISCONNECTED);
    }

    @Override public void reconnectionSuccessful() {
      if (Logging.ENABLED) {
        Log.d(TAG, "Reconnection success");
      }
    }

    @Override public void reconnectingIn(int seconds) {
      if (Logging.ENABLED) {
        Log.d(TAG, "Reconnecting in " + seconds);
      }
    }

    @Override public void reconnectionFailed(Exception e) {
      if (Logging.ENABLED) {
        Log.d(TAG, "reconnection failed");
      }
    }
  };
  private String username;
  private String password;
  private Context context;

  private ChatMessageListener chatMessageListener = (chat, message) -> {
    messageSubject.onNext(message);
  };

  private MessageListener messageListener = message -> {
    String confUsername = TextUtils.split(message.getFrom(), "/")[1];
    if (!(Objects.equals(confUsername, username))) {
      messageSubject.onNext(message);
    }
  };

  public RxXMPP(XMPPConfig xmppConfig, SharedPreferences preferences, Context context) {
    this.xmppConfig = xmppConfig;
    this.preferences = preferences;
    this.context = context;
    username = preferences.getString("drip_username", null);
    password = preferences.getString("drip_password", null);
    authenticatedNotification = authenticatedSubject.filter(state -> state == CONNECTED);
  }

  private void createConnection() {
    XMPPTCPConnectionConfiguration.Builder configBuilder = XMPPTCPConnectionConfiguration.builder();
    configBuilder.setDebuggerEnabled(Logging.ENABLED);
    configBuilder.setSecurityMode(ConnectionConfiguration.SecurityMode.disabled);
    configBuilder.setDebuggerEnabled(true);
    configBuilder.setResource(xmppConfig.getResource());
    configBuilder.setServiceName(xmppConfig.getDomain());
    configBuilder.setHost(xmppConfig.getHost());
    configBuilder.setPort(xmppConfig.getPort());
    XMPPTCPConnection.setUseStreamManagementResumptionDefault(false);
    XMPPTCPConnection.setUseStreamManagementDefault(true);

    connection = new XMPPTCPConnection(configBuilder.build());
    ReconnectionManager.setEnabledPerDefault(true);
    ReconnectionManager.setDefaultFixedDelay(xmppConfig.getReconnectionDelay());

    connection.addConnectionListener(connectionListener);
  }

  private synchronized void ensureConnection() throws IOException, XMPPException, SmackException {
    if (connection == null) {
      createConnection();
    }
    if (connection != null && !connection.isConnected()) {
      connect();
    }
  }

  private void connect() throws IOException, XMPPException, SmackException {
    connection.connect();
  }

  private void ensureAuthenticated() throws XMPPException, IOException, SmackException {
    ensureConnection();
    if (!connection.isAuthenticated()) {
      if (username != null && password != null) {
        login(preferences.getString("drip_username", null),
            preferences.getString("drip_password", null));
      }
    }
  }

  public void getUserData() {
    try {
      ensureAuthenticated();
      VCard vCard = vCardManager.loadVCard();
      EventBus.getDefault().post(new VCardLoaded(vCard));
    } catch (IOException | XMPPException | SmackException e) {
      e.printStackTrace();
    }
  }

  public VCard getVcardForUser(String jid) {
    try {
      ensureConnection();
      return vCardManager.loadVCard(jid);
    } catch (IOException | XMPPException | SmackException e) {
      e.printStackTrace();
    }

    return null;
  }

  public boolean isAuthenticated() {
    return connection != null && connection.isConnected() && connection.isAuthenticated();
  }

  public void addGroup(String topic) {
    try {
      ensureAuthenticated();
      MultiUserChatManager multiUserChatManager = MultiUserChatManager.getInstanceFor(connection);
      MultiUserChat multiUserChat =
          multiUserChatManager.getMultiUserChat(topic + "@conference.drip");
      DiscussionHistory discussionHistory = new DiscussionHistory();
      discussionHistory.setMaxStanzas(0);
      multiUserChat.createOrJoin(preferences.getString("drip_username", null), null,
          discussionHistory, 60000);
      multiUserChat.sendConfigurationForm(new Form(DataForm.Type.submit));
      EventBus.getDefault().post(new AddGroupSuccess(multiUserChat.getRoom()));
    } catch (XMPPException | IOException | SmackException e) {
      e.printStackTrace();
      EventBus.getDefault().post(new AddGroupFailed());
    }
  }

  public void addContact(String userJid) {
    try {
      ensureAuthenticated();
      Roster roster = Roster.getInstanceFor(connection);
      if (!roster.contains(userJid)) {
        roster.createEntry(userJid, null, null);
      }
      EventBus.getDefault().post(new AddFriendSuccess());
    } catch (XMPPException | IOException | SmackException e) {
      e.printStackTrace();
      EventBus.getDefault().post(new AddFriendFailed());
    }
  }

  public void getContacts() {
    try {
      ensureAuthenticated();
      Roster roster = Roster.getInstanceFor(connection);
      Collection<RosterEntry> contacts = roster.getEntries();
      SolidSet<RosterEntry> rosterEntries = new SolidSet<>(contacts);
      List<Contact> contactSolidList = rosterEntries.map(e -> {
        VCard vCard = getVcardForUser(e.getUser());
        if (vCard != null) {
          return new Contact(e.getUser(), vCard.getNickName(), vCard.getAvatar());
        }

        return null;
      }).filter(e -> e != null).collect(toList());
      EventBus.getDefault().post(new ContactLoaded(contactSolidList));
    } catch (XMPPException | IOException | SmackException e) {
      e.printStackTrace();
    }
  }

  public void inviteUsersToGroup(String room, CharSequence[] users) {
    try {
      ensureConnection();
      MultiUserChatManager multiUserChatManager = MultiUserChatManager.getInstanceFor(connection);
      MultiUserChat multiUserChat = multiUserChatManager.getMultiUserChat(room);
      Stream.of(users).map(CharSequence::toString).forEach((Action1<? super String>) value -> {
        try {
          multiUserChat.invite(value, "hei join ya!");
        } catch (SmackException.NotConnectedException e) {
          e.printStackTrace();
        }
      });
    } catch (IOException | XMPPException | SmackException e) {
      e.printStackTrace();
    }
  }

  public void login(String username, String password) {
    try {
      ensureConnection();
      connection.login(username, password, "Android-" + username);
      Presence presence = new Presence(Presence.Type.available);
      connection.sendStanza(presence);
      VCardManager vCardManager = VCardManager.getInstanceFor(connection);
      VCard vCard = new VCard();
      vCard.setNickName(username);
      vCardManager.saveVCard(vCard);
      preferences.edit().putString("drip_username", username).apply();
      preferences.edit().putString("drip_password", password).apply();
      preferences.edit().putString("drip_jabber_id", connection.getUser()).apply();
      EventBus.getDefault().post(new XmppAuthenticationSuccess(username, password));
    } catch (IOException | XMPPException | SmackException e) {
      e.printStackTrace();
      connection.disconnect();
      connection = null;
      EventBus.getDefault().post(new XmppAuthenticationError());
    }
  }

  public void register(String username, String password) {
    try {
      ensureConnection();
      AccountManager accountManager = AccountManager.getInstance(connection);
      accountManager.sensitiveOperationOverInsecureConnection(true);
      accountManager.createAccount(username, password);
      login(username, password);
    } catch (IOException | XMPPException | SmackException e) {
      e.printStackTrace();
    }
  }

  public Observable<Integer> authenticationSubject() {
    return authenticatedSubject;
  }

  public Observable<Message> getNewMessages(List<ChatModel> groups) {
    return Observable.create(subscriber -> {
      try {
        ensureConnection();
      } catch (IOException | XMPPException | SmackException e) {
        e.printStackTrace();
        subscriber.onError(e);
      }

      subscriber.add(Subscriptions.create(() -> connection.disconnect()));
      subscriber.add(messageSubject.subscribe(subscriber));
      startListenChatMessages(groups);
    });
  }

  public Observable<Message> sendMessage(Message message) {
    return Observable.<Message>fromEmitter(emitter -> {
      try {
        ensureConnection();
        if (connection != null && connection.isConnected() && connection.isAuthenticated()) {
          Message.Type type = message.getType();
          connection.addStanzaIdAcknowledgedListener(message.getStanzaId(), packet -> {
            if (Logging.ENABLED) {
              Log.i(TAG, "processpacket: " + packet.toString());
            }
            emitter.onNext(message);
            emitter.onCompleted();
          });
          if (type == Message.Type.chat) {
            ChatManager manager = ChatManager.getInstanceFor(connection);
            manager.createChat(message.getTo()).sendMessage(message);
          } else if (type == Message.Type.groupchat) {
            MultiUserChatManager multiUserChatManager =
                MultiUserChatManager.getInstanceFor(connection);
            MultiUserChat multiUserChat = multiUserChatManager.getMultiUserChat(message.getTo());
            if (!multiUserChat.isJoined()) {
              DiscussionHistory discussionHistory = new DiscussionHistory();
              discussionHistory.setMaxStanzas(0);
              multiUserChat.join(username, null, discussionHistory, 60000);
            }
            multiUserChat.sendMessage(message);
          }
        }
      } catch (IOException | XMPPException | SmackException e) {
        e.printStackTrace();
        emitter.onError(e);
      }
    }, Emitter.BackpressureMode.BUFFER).timeout(xmppConfig.getSendMessageTimeout(),
        TimeUnit.SECONDS).delaySubscription(authenticatedNotification);
  }

  public void startListenMucMessages(String room) {
    MultiUserChatManager multiUserChatManager = MultiUserChatManager.getInstanceFor(connection);
    MultiUserChat multiUserChat = multiUserChatManager.getMultiUserChat(room);
    if (!multiUserChat.isJoined()) {
      DiscussionHistory discussionHistory = new DiscussionHistory();
      discussionHistory.setMaxStanzas(0);
      try {
        multiUserChat.join(username, null, discussionHistory, 60000);
      } catch (SmackException.NoResponseException | XMPPException.XMPPErrorException | SmackException.NotConnectedException e) {
        e.printStackTrace();
      }
    }
    multiUserChat.removeMessageListener(messageListener);
    multiUserChat.addMessageListener(messageListener);
  }

  private void startListenChatMessages(List<ChatModel> groups) {
    ChatManager chatManager = ChatManager.getInstanceFor(connection);
    chatManager.addChatListener(
        ((chat, createdLocally) -> chat.addMessageListener(chatMessageListener)));
    MultiUserChatManager multiUserChatManager = MultiUserChatManager.getInstanceFor(connection);
    Stream.stream(groups).forEach((Action1<? super ChatModel>) chatModel -> {
      MultiUserChat multiUserChat = multiUserChatManager.getMultiUserChat(chatModel.getJid());
      if (!multiUserChat.isJoined()) {
        DiscussionHistory discussionHistory = new DiscussionHistory();
        discussionHistory.setMaxStanzas(0);
        try {
          multiUserChat.join(username, null, discussionHistory, 60000);
        } catch (SmackException.NoResponseException | XMPPException.XMPPErrorException | SmackException.NotConnectedException e) {
          e.printStackTrace();
        }
      }
      multiUserChat.removeMessageListener(messageListener);
      multiUserChat.addMessageListener(messageListener);
    });
  }
}
