package me.ndjoe.dripmessenger.data.xmpp;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import br.com.goncalves.pugnotification.notification.PugNotification;
import com.sjl.foreground.Foreground;
import java.util.List;
import javax.inject.Inject;
import me.ndjoe.dripmessenger.DripMessengerComponent;
import me.ndjoe.dripmessenger.R;
import me.ndjoe.dripmessenger.data.Logging;
import me.ndjoe.dripmessenger.data.XMPPConfig;
import me.ndjoe.dripmessenger.data.local.chat.ChatRepository;
import me.ndjoe.dripmessenger.data.local.chat.criteria.GroupCriteria;
import me.ndjoe.dripmessenger.data.local.chat.model.ChatModel;
import me.ndjoe.dripmessenger.data.local.message.MessagesRepository;
import me.ndjoe.dripmessenger.data.local.message.model.MessageModel;
import me.ndjoe.dripmessenger.data.local.message.model.MessageState;
import me.ndjoe.dripmessenger.event.AddFriend;
import me.ndjoe.dripmessenger.event.AddFriendSuccess;
import me.ndjoe.dripmessenger.event.AddGroup;
import me.ndjoe.dripmessenger.event.AddGroupSuccess;
import me.ndjoe.dripmessenger.event.ContactLoaded;
import me.ndjoe.dripmessenger.event.GetContact;
import me.ndjoe.dripmessenger.event.InviteUsersToGroup;
import me.ndjoe.dripmessenger.event.LoginXmpp;
import me.ndjoe.dripmessenger.event.RegisterUser;
import me.ndjoe.dripmessenger.event.XmppAuthenticationSuccess;
import me.ndjoe.dripmessenger.event.XmppGetUserData;
import me.ndjoe.dripmessenger.injection.Injector;
import me.ndjoe.dripmessenger.recycleradapter.model.Contact;
import me.ndjoe.dripmessenger.utils.ObserverAdapter;
import me.ndjoe.dripmessenger.utils.RxUtils;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.jivesoftware.smack.packet.Message;
import rx.Observable;
import rx.Subscription;
import rx.schedulers.Schedulers;
import solid.functions.Action1;
import solid.stream.Stream;

public class XMPPService extends Service {
  private static final String KEY_XMPP_CONFIGURATION = "xmpp_configuration";
  private static final String ACTION_STOP = "stop_xmpp";
  private static XMPPService instance;
  private final int NOTIFICATION_ID = 9898;
  @Inject MessagesRepository messagesRepository;
  @Inject ChatRepository chatRepository;
  @Inject SharedPreferences preferences;
  Subscription newMessagesSubscription;
  Subscription notifyMessageUpdateSubscription;
  Subscription groupMessageSubscription;
  private RxXMPP rxXMPP;
  private BroadcastReceiver stopReceiver = new BroadcastReceiver() {
    @Override public void onReceive(Context context, Intent intent) {
      stopSelf();
    }
  };

  public static XMPPService getInstance() {
    return instance;
  }

  public static void run(Context context, XMPPConfig config) {
    Intent intent = new Intent(context, XMPPService.class);
    intent.putExtra(KEY_XMPP_CONFIGURATION, config);
    context.startService(intent);
  }

  public static void stop(Context context) {
    LocalBroadcastManager broadcastManager = LocalBroadcastManager.getInstance(context);
    broadcastManager.sendBroadcast(new Intent(ACTION_STOP));
  }

  @Override public void onCreate() {
    super.onCreate();
    Injector.getComponent(getApplication(), DripMessengerComponent.class).inject(this);
    instance = this;
    EventBus.getDefault().register(this);
    LocalBroadcastManager.getInstance(this)
        .registerReceiver(stopReceiver, new IntentFilter(ACTION_STOP));
  }

  @Override public void onDestroy() {
    super.onDestroy();
    instance = null;
    EventBus.getDefault().unregister(this);
    LocalBroadcastManager.getInstance(this).unregisterReceiver(stopReceiver);
    notifyMessageUpdateSubscription.unsubscribe();
    newMessagesSubscription.unsubscribe();
    RxUtils.unsubscribe(this);
  }

  @Override public int onStartCommand(Intent intent, int flags, int startId) {
    XMPPConfig config = new XMPPConfig("128.199.220.162", 5222, "drip", "drip-android");
    openXmppConnection(config);
    Log.d("xmpp service", "service running");

    return START_STICKY;
  }

  @Subscribe(threadMode = ThreadMode.BACKGROUND) public void loginUser(LoginXmpp e) {
    rxXMPP.login(e.getUsername(), e.getPassword());
  }

  @Subscribe(threadMode = ThreadMode.BACKGROUND) public void loadUserData(XmppGetUserData e) {
    rxXMPP.getUserData();
  }

  @Subscribe(threadMode = ThreadMode.BACKGROUND) public void getContacts(GetContact e) {
    rxXMPP.getContacts();
  }

  @Subscribe(threadMode = ThreadMode.BACKGROUND)
  public void onInviteUsersToGroup(InviteUsersToGroup e) {
    rxXMPP.inviteUsersToGroup(e.getRoom(), e.getUsers());
  }

  @Subscribe(threadMode = ThreadMode.BACKGROUND) public void addContact(AddFriend e) {
    rxXMPP.addContact(e.getUserJid() + "@drip");
  }

  @Subscribe(threadMode = ThreadMode.BACKGROUND) public void addContactSuccess(AddFriendSuccess e) {
    rxXMPP.getContacts();
  }

  @Subscribe(threadMode = ThreadMode.BACKGROUND) public void registerUser(RegisterUser e) {
    rxXMPP.register(e.getUsername(), e.getPassword());
  }

  @Subscribe(threadMode = ThreadMode.BACKGROUND) public void addGroup(AddGroup e) {
    rxXMPP.addGroup(e.getTopic());
  }

  @Subscribe public void addGroupSuccess(AddGroupSuccess e) {
    chatRepository.put(new ChatModel(e.getName(), e.getName(), "groupchat"))
        .subscribe(chatModel -> {
          Log.d("chat model added", chatModel.toString());
        });
    rxXMPP.startListenMucMessages(e.getName());
  }

  @Subscribe public void contactsLoaded(ContactLoaded e) {
    Stream.stream(e.getContacts())
        .forEach((Action1<? super Contact>) contact -> chatRepository.put(
            new ChatModel(contact.getJid(), contact.getNickname())).subscribe(chatModel -> {
          Log.d("chat model added", chatModel.toString());
        }));
  }

  @Subscribe public void onAuthenticated(XmppAuthenticationSuccess e) {
    subscribeMessages();
  }

  public Observable<Integer> getAuthenticationState() {
    return rxXMPP.authenticationSubject();
  }

  private void openXmppConnection(XMPPConfig config) {
    if (rxXMPP != null) return;

    rxXMPP = new RxXMPP(config, preferences, this);

    if (rxXMPP.isAuthenticated()) {
      subscribeMessages();
    }
  }

  private void subscribeMessages() {
    if (newMessagesSubscription != null) {
      newMessagesSubscription.unsubscribe();
    }
    List<ChatModel> groups = chatRepository.getChats(GroupCriteria.create());

    newMessagesSubscription = rxXMPP.getNewMessages(groups).flatMap(xmppMessage -> {
      MessageModel messageModel = Mapper.map(xmppMessage);

      if (messageModel != null) {
        Log.d("new-message", messageModel.toString());
        return chatRepository.getByIdRaw(messageModel.getChatId())
            .flatMap(chatDtos -> {
              if (chatDtos.size() > 0) {
                return Observable.just(chatDtos.get(0));
              }

              return chatRepository.put(
                  new ChatModel(messageModel.getFromUser(), messageModel.getFromUser(),
                      messageModel.getType() == Message.Type.chat ? "chat" : "groupchat"));
            })
            .flatMap(
                o -> messagesRepository.put(messageModel).onErrorResumeNext(Observable.empty()))
            .doOnNext(model -> {
              if (Foreground.get().isBackground()) {
                pushNotif(model);
              }
            });
      } else {
        return Observable.empty();
      }
    }).retry().subscribeOn(Schedulers.io()).subscribe(new ObserverAdapter<MessageModel>() {
      @Override public void onCompleted() {
        super.onCompleted();
      }

      @Override public void onError(Throwable e) {
        super.onError(e);
        if (Logging.ENABLED) {
          e.printStackTrace();
        }
      }
    });

    if (notifyMessageUpdateSubscription != null) {
      notifyMessageUpdateSubscription.unsubscribe();
    }

    notifyMessageUpdateSubscription = messagesRepository.notifyUpdates()
        .filter(messageModel -> messageModel.getState() == MessageState.STATE_OUTGOING)
        .flatMap(messageModel -> {
          messageModel.setState(MessageState.STATE_SENDING);
          return messagesRepository.put(messageModel);
        })
        .flatMap(messageModel -> {
          Message message = Mapper.map(messageModel);

          return rxXMPP.sendMessage(message).flatMap(xmppMessage -> {
            messageModel.setState(MessageState.STATE_SENT);
            return messagesRepository.put(messageModel);
          }).onErrorResumeNext(throwable -> {
            messageModel.setState(MessageState.STATE_ERROR);
            return messagesRepository.put(messageModel);
          });
        })
        .subscribeOn(Schedulers.io())
        .subscribe(ObserverAdapter.empty());
    Log.d("subscribe message", "message subscribing");
  }

  private void pushNotif(MessageModel model) {
    PugNotification.with(this)
        .load()
        .smallIcon(R.mipmap.ic_launcher)
        .title(model.getFromUser())
        .message(model.getBody())
        .simple()
        .build();
  }

  @Nullable @Override public IBinder onBind(Intent intent) {
    return null;
  }
}
