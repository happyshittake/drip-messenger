package me.ndjoe.dripmessenger.data.di.module;

import dagger.Module;
import dagger.Provides;
import javax.inject.Singleton;
import me.ndjoe.dripmessenger.data.local.chat.ChatRepository;
import me.ndjoe.dripmessenger.data.local.chat.LocalChatRepository;
import me.ndjoe.dripmessenger.data.local.message.LocalMessageRepository;
import me.ndjoe.dripmessenger.data.local.message.MessagesRepository;

@Module public class RepoModule {

  @Provides @Singleton public MessagesRepository provideMessagesRepository(
      LocalMessageRepository localMessageRepository) {
    return localMessageRepository;
  }

  @Provides @Singleton
  public ChatRepository provideChatRepository(LocalChatRepository localChatRepository) {
    return localChatRepository;
  }
}
