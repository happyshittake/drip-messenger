package me.ndjoe.dripmessenger.data.local.chat.criteria;

import com.pushtorefresh.storio.sqlite.queries.Query;
import me.ndjoe.dripmessenger.data.local.common.QueryCriteria;
import me.ndjoe.dripmessenger.data.local.db.table.ChatTable;

/**
 * Created by ndjoe on 07/12/16.
 */

public class EmptyCriteria extends QueryCriteria {
  private EmptyCriteria() {

  }

  public static EmptyCriteria create() {
    return new EmptyCriteria();
  }

  @Override public Query filter() {
    return Query.builder().table(ChatTable.TABLE).build();
  }
}
