package me.ndjoe.dripmessenger.data.local;

import java.util.List;
import javax.inject.Inject;
import me.ndjoe.dripmessenger.data.local.chat.ChatRepository;
import me.ndjoe.dripmessenger.data.local.chat.criteria.EmptyCriteria;
import me.ndjoe.dripmessenger.data.local.chat.model.ChatModel;
import me.ndjoe.dripmessenger.data.local.message.MessagesRepository;
import me.ndjoe.dripmessenger.data.local.message.criteria.MessageStateCriteria;
import me.ndjoe.dripmessenger.data.local.message.criteria.MessagesOffsetCriteria;
import me.ndjoe.dripmessenger.data.local.message.model.MessageState;
import rx.Observable;
import rx.Subscription;
import rx.subjects.PublishSubject;
import rx.subscriptions.CompositeSubscription;

public class ChatInteractor {
  private ChatRepository chatRepository;
  private MessagesRepository messagesRepository;

  private CompositeSubscription compositeSubscription = new CompositeSubscription();
  private PublishSubject<Object> notificationSubject = PublishSubject.create();

  @Inject
  public ChatInteractor(ChatRepository chatRepository, MessagesRepository messagesRepository) {
    this.chatRepository = chatRepository;
    this.messagesRepository = messagesRepository;
  }

  public Observable<List<ChatModel>> getChats() {
    return chatRepository.get(EmptyCriteria.create())
        .doOnSubscribe(() -> compositeSubscription.clear())
        .doOnNext(chatModel -> {
          Subscription subscription =
              messagesRepository.notifyUpdates(chatModel.getJid()).filter(messageModel -> {
                int state = messageModel.getState();
                return state == MessageState.STATE_NEW
                    || state == MessageState.STATE_OUTGOING
                    || state == MessageState.STATE_SENDING
                    || state == MessageState.STATE_READ;
              }).subscribe(messageModel -> notificationSubject.onNext(null));
          compositeSubscription.add(subscription);
        })
        .flatMap(chatModel -> messagesRepository.get(
            MessageStateCriteria.create(chatModel.getJid(), MessageState.STATE_NEW))
            .map(messageModels -> {
              chatModel.setUnreadMessage(messageModels.size());
              return chatModel;
            })
            .defaultIfEmpty(chatModel))
        .flatMap(chatModel -> messagesRepository.get(
            MessagesOffsetCriteria.create(chatModel.getJid(), 0, 1)).map(messageModels -> {
          if (!messageModels.isEmpty()) {
            chatModel.setLastMessage(messageModels.get(0));
          }

          return chatModel;
        }).defaultIfEmpty(chatModel))
        .toList()
        .repeatWhen(observable -> observable.flatMap(o -> notificationSubject.take(1)));
  }

  public Observable<ChatModel> addChat(ChatModel chatModel) {
    return chatRepository.put(chatModel);
  }

  public Observable<ChatModel> removeChat(ChatModel chatModel) {
    return chatRepository.remove(chatModel);
  }

  public Observable<Void> clear() {
    return chatRepository.clear();
  }
}
