package me.ndjoe.dripmessenger.data.local.chat.dto;

import com.pushtorefresh.storio.sqlite.annotations.StorIOSQLiteColumn;
import com.pushtorefresh.storio.sqlite.annotations.StorIOSQLiteType;
import me.ndjoe.dripmessenger.data.local.db.table.ChatTable;

/**
 * Created by ndjoe on 07/12/16.
 */

@StorIOSQLiteType(table = ChatTable.TABLE) public class ChatDto {
  @StorIOSQLiteColumn(name = ChatTable.COLUMN_JID, key = true) public String jid;
  @StorIOSQLiteColumn(name = ChatTable.COLUMN_NAME) public String name;
  @StorIOSQLiteColumn(name = ChatTable.COLUMN_TYPE) public String type;
}
