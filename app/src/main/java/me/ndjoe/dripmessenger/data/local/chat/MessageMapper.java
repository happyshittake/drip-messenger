package me.ndjoe.dripmessenger.data.local.chat;

import android.support.annotation.NonNull;
import java.util.Calendar;
import me.ndjoe.dripmessenger.data.local.chat.model.ChatModel;
import me.ndjoe.dripmessenger.data.local.message.model.MessageModel;
import me.ndjoe.dripmessenger.data.local.message.model.MessageState;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.id.StanzaIdUtil;

/**
 * Created by ndjoe on 07/12/16.
 */

public class MessageMapper {

  @NonNull
  public static MessageModel map(String text, ChatModel currentChat, String fromUser, String type) {
    MessageModel messageModel = new MessageModel(nextMessageId(), null, MessageState.STATE_OUTGOING,
        Calendar.getInstance().getTime(), fromUser, currentChat.getJid(), currentChat.getJid());
    messageModel.setBody(text);
    messageModel.setType(Message.Type.fromString(type));
    return messageModel;
  }

  public static String nextMessageId() {
    return StanzaIdUtil.newStanzaId();
  }
}
