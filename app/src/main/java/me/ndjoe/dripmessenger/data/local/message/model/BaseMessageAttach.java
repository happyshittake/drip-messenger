package me.ndjoe.dripmessenger.data.local.message.model;

/**
 * Created by ndjoe on 07/12/16.
 */

public abstract class BaseMessageAttach {
  public abstract String getJson();
}
