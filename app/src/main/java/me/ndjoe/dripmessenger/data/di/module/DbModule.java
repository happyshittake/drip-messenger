package me.ndjoe.dripmessenger.data.di.module;

import com.pushtorefresh.storio.sqlite.SQLiteTypeMapping;
import com.pushtorefresh.storio.sqlite.StorIOSQLite;
import com.pushtorefresh.storio.sqlite.impl.DefaultStorIOSQLite;
import dagger.Module;
import dagger.Provides;
import javax.inject.Singleton;
import me.ndjoe.dripmessenger.data.di.Internal;
import me.ndjoe.dripmessenger.data.local.chat.dto.ChatDto;
import me.ndjoe.dripmessenger.data.local.chat.dto.ChatDtoSQLiteTypeMapping;
import me.ndjoe.dripmessenger.data.local.db.DbOpenHelper;
import me.ndjoe.dripmessenger.data.local.message.dto.MessageDto;
import me.ndjoe.dripmessenger.data.local.message.dto.MessageDtoSQLiteTypeMapping;
import me.ndjoe.dripmessenger.data.local.message.model.MessageModel;
import me.ndjoe.dripmessenger.data.local.message.resolvers.MessageDeleteResolver;
import me.ndjoe.dripmessenger.data.local.message.resolvers.MessageGetResolver;
import me.ndjoe.dripmessenger.data.local.message.resolvers.MessagePutResolver;

@Module public class DbModule {
  @Provides @Singleton @Internal
  public StorIOSQLite provideStorIOSQLite(DbOpenHelper dbOpenHelper) {
    return DefaultStorIOSQLite.builder()
        .sqliteOpenHelper(dbOpenHelper)
        .addTypeMapping(MessageDto.class, new MessageDtoSQLiteTypeMapping())
        .addTypeMapping(ChatDto.class, new ChatDtoSQLiteTypeMapping())
        .addTypeMapping(MessageModel.class,
            SQLiteTypeMapping.<MessageModel>builder().putResolver(new MessagePutResolver())
                .getResolver(new MessageGetResolver())
                .deleteResolver(new MessageDeleteResolver())
                .build())
        .build();
  }
}
