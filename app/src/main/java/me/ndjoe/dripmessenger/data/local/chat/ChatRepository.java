package me.ndjoe.dripmessenger.data.local.chat;

import java.util.List;
import me.ndjoe.dripmessenger.data.local.chat.dto.ChatDto;
import me.ndjoe.dripmessenger.data.local.chat.model.ChatModel;
import me.ndjoe.dripmessenger.data.local.common.Criteria;
import rx.Observable;

/**
 * Created by ndjoe on 07/12/16.
 */

public interface ChatRepository {
  Observable<ChatModel> get(Criteria criteria);

  Observable<ChatModel> put(ChatModel chatModel);

  Observable<ChatModel> remove(ChatModel chatModel);

  Observable<Void> clear();

  Observable<List<ChatDto>> getByIdRaw(String chatId);

  List<ChatModel> getChats(Criteria criteria);
}
