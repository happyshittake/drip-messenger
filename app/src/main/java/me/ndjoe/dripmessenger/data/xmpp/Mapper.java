package me.ndjoe.dripmessenger.data.xmpp;

import android.text.TextUtils;
import java.util.Date;
import me.ndjoe.dripmessenger.data.local.message.model.BaseMessageAttach;
import me.ndjoe.dripmessenger.data.local.message.model.MessageModel;
import me.ndjoe.dripmessenger.data.local.message.model.MessageState;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smackx.delay.packet.DelayInformation;
import org.jivesoftware.smackx.json.packet.JsonPacketExtension;

/**
 * Created by ndjoe on 13/12/16.
 */

public class Mapper {
  static Message map(MessageModel messageModel) {
    String to = messageModel.getToUser();
    Message.Type type = messageModel.getType();

    Message message = new Message(to, messageModel.getBody());

    message.setType(type);

    message.setStanzaId(messageModel.getId());
    message.setFrom(messageModel.getFromUser());

    BaseMessageAttach attach = messageModel.getAttach();

    if (attach != null) {
      message.addExtension(new JsonPacketExtension(attach.getJson()));
    }

    return message;
  }

  static MessageModel map(Message xmppMessage) {
    Date messageTime;
    DelayInformation delayInformation =
        xmppMessage.getExtension(DelayInformation.ELEMENT, DelayInformation.NAMESPACE);
    if (delayInformation != null) {
      messageTime = delayInformation.getStamp();
    } else {
      messageTime = new Date();
    }

    String messageId = xmppMessage.getStanzaId();

    if (TextUtils.isEmpty(messageId)) {
      return null;
    }

    final String messageInfoJson;
    JsonPacketExtension jsonPacketExtension =
        xmppMessage.getExtension(JsonPacketExtension.ELEMENT, JsonPacketExtension.NAMESPACE);
    if (jsonPacketExtension != null) {
      messageInfoJson = jsonPacketExtension.getJson();
    } else {
      messageInfoJson = null;
    }

    DelayInformation delayInfo = DelayInformation.from(xmppMessage);
    String from = "unknown";
    MessageModel messageModel = null;
    if (xmppMessage.getType() == Message.Type.chat) {
      from = TextUtils.split(xmppMessage.getFrom(), "/")[0];
      messageModel = new MessageModel(messageId, null, MessageState.STATE_NEW, messageTime, from,
          xmppMessage.getTo(), from, delayInfo != null, xmppMessage.getType());
      messageModel.setBody(xmppMessage.getBody());
    } else if (xmppMessage.getType() == Message.Type.groupchat) {
      String[] fromChat = TextUtils.split(xmppMessage.getFrom(), "/");
      messageModel =
          new MessageModel(messageId, null, MessageState.STATE_NEW, messageTime, fromChat[1],
              xmppMessage.getTo(), fromChat[0], delayInfo != null, xmppMessage.getType());
      messageModel.setBody(xmppMessage.getBody());
    }

    return messageModel;
  }
}
