package me.ndjoe.dripmessenger.data.local.chat;

import me.ndjoe.dripmessenger.data.local.chat.dto.ChatDto;
import me.ndjoe.dripmessenger.data.local.chat.model.ChatModel;

/**
 * Created by ndjoe on 07/12/16.
 */

public class ChatMapper {
  static ChatModel map(ChatDto dto) {
    return new ChatModel(dto.jid, dto.name, dto.type);
  }

  public static ChatDto map(ChatModel model) {
    ChatDto dto = new ChatDto();
    dto.jid = model.getJid();
    dto.name = model.getName();
    dto.type = model.getType();
    return dto;
  }
}
