package me.ndjoe.dripmessenger.data.local.chat.criteria;

import com.pushtorefresh.storio.sqlite.queries.Query;
import me.ndjoe.dripmessenger.data.local.common.QueryCriteria;
import me.ndjoe.dripmessenger.data.local.db.table.ChatTable;

public class GroupCriteria extends QueryCriteria {

  public static GroupCriteria create() {
    return new GroupCriteria();
  }

  @Override public Query filter() {
    return Query.builder()
        .table(ChatTable.TABLE)
        .where(ChatTable.COLUMN_TYPE + " = ?")
        .whereArgs("groupchat")
        .build();
  }
}