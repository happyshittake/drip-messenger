package me.ndjoe.dripmessenger.data.local.message.model;

import java.util.Date;
import me.ndjoe.dripmessenger.data.local.common.BaseModel;
import org.jivesoftware.smack.packet.Message;

/**
 * Created by ndjoe on 07/12/16.
 */

public class MessageModel extends BaseModel {
  private String id;
  private Integer weight;
  private @MessageState.State int state;
  private Date date;
  private String fromUser;
  private String toUser;
  private String body;
  private String chatId;
  private BaseMessageAttach attach;
  private boolean offline;
  private Message.Type type;

  public MessageModel(String id, Integer weight, int state, Date date, String fromUser,
      String toUser, String body, String chatId, BaseMessageAttach attach, boolean offline) {
    this.id = id;
    this.weight = weight;
    this.state = state;
    this.date = date;
    this.fromUser = fromUser;
    this.toUser = toUser;
    this.body = body;
    this.chatId = chatId;
    this.attach = attach;
    this.offline = offline;
    this.type = Message.Type.chat;
  }

  public MessageModel(String id, Integer weight, int state, Date date, String fromUser,
      String toUser, String chatId) {
    this.id = id;
    this.weight = weight;
    this.state = state;
    this.date = date;
    this.fromUser = fromUser;
    this.toUser = toUser;
    this.body = body;
    this.chatId = chatId;
    this.type = Message.Type.chat;
  }

  public MessageModel(String id, Integer weight, int state, Date date, String fromUser,
      String toUser, String chatId, Boolean offline, Message.Type type) {
    this.id = id;
    this.weight = weight;
    this.state = state;
    this.date = date;
    this.fromUser = fromUser;
    this.toUser = toUser;
    this.chatId = chatId;
    this.offline = offline;
    this.type = type;
  }

  public Message.Type getType() {
    return type;
  }

  public void setType(Message.Type type) {
    this.type = type;
  }

  public MessageModel(String id, Integer weight, int state, Date date, String fromUser,
      String toUser, String chatId, Boolean offline) {
    this.id = id;
    this.weight = weight;
    this.state = state;
    this.date = date;
    this.fromUser = fromUser;
    this.toUser = toUser;
    this.chatId = chatId;
    this.offline = offline;
    this.type = Message.Type.chat;
  }

  @Override public String toString() {
    return "MessageModel{" +
        "id='" + id + '\'' +
        ", weight=" + weight +
        ", state=" + state +
        ", date=" + date +
        ", fromUser='" + fromUser + '\'' +
        ", toUser='" + toUser + '\'' +
        ", body='" + body + '\'' +
        ", chatId='" + chatId + '\'' +
        ", attach=" + attach +
        ", offline=" + offline +
        '}';
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public Integer getWeight() {
    return weight;
  }

  public void setWeight(Integer weight) {
    this.weight = weight;
  }

  public int getState() {
    return state;
  }

  public void setState(int state) {
    this.state = state;
  }

  public Date getDate() {
    return date;
  }

  public void setDate(Date date) {
    this.date = date;
  }

  public String getFromUser() {
    return fromUser;
  }

  public void setFromUser(String fromUser) {
    this.fromUser = fromUser;
  }

  public String getToUser() {
    return toUser;
  }

  public void setToUser(String toUser) {
    this.toUser = toUser;
  }

  public String getBody() {
    return body;
  }

  public void setBody(String body) {
    this.body = body;
  }

  public String getChatId() {
    return chatId;
  }

  public void setChatId(String chatId) {
    this.chatId = chatId;
  }

  public BaseMessageAttach getAttach() {
    return attach;
  }

  public void setAttach(BaseMessageAttach attach) {
    this.attach = attach;
  }

  public boolean isOffline() {
    return offline;
  }

  public void setOffline(boolean offline) {
    this.offline = offline;
  }
}
