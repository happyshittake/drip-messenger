package me.ndjoe.dripmessenger.data.local.chat.model;

import me.ndjoe.dripmessenger.data.local.common.BaseModel;
import me.ndjoe.dripmessenger.data.local.message.model.MessageModel;

/**
 * Created by ndjoe on 07/12/16.
 */

public class ChatModel extends BaseModel {
  private String jid;
  private String name;
  private String type;
  private int unreadMessage;
  private MessageModel lastMessage;

  public ChatModel(String jid, String name) {
    this.jid = jid;
    this.name = name;
    this.type = "chat";
  }

  public ChatModel(String jid, String name, String type) {
    this.jid = jid;
    this.name = name;
    this.type = type == null ? "chat" : type;
  }

  @Override public String toString() {
    return "ChatModel{" +
        "jid='" + jid + '\'' +
        ", name='" + name + '\'' +
        ", unreadMessage=" + unreadMessage +
        ", lastMessage=" + lastMessage +
        '}';
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public String getJid() {
    return jid;
  }

  public void setJid(String jid) {
    this.jid = jid;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getUnreadMessage() {
    return unreadMessage;
  }

  public void setUnreadMessage(int unreadMessage) {
    this.unreadMessage = unreadMessage;
  }

  public MessageModel getLastMessage() {
    return lastMessage;
  }

  public void setLastMessage(MessageModel lastMessage) {
    this.lastMessage = lastMessage;
  }
}
