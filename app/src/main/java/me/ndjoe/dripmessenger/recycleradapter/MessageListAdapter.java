package me.ndjoe.dripmessenger.recycleradapter;

import com.bumptech.glide.Glide;
import com.chad.library.adapter.base.BaseMultiItemQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import java.util.List;
import me.ndjoe.dripmessenger.R;
import me.ndjoe.dripmessenger.recycleradapter.model.MessageAdapterItem;

public class MessageListAdapter
    extends BaseMultiItemQuickAdapter<MessageAdapterItem, BaseViewHolder> {
  public MessageListAdapter(List<MessageAdapterItem> data) {
    super(data);
    addItemType(MessageAdapterItem.SELF_TEXT, R.layout.layout_self_message_item);
    addItemType(MessageAdapterItem.OTHER_TEXT, R.layout.layout_other_message_item);
  }

  @Override
  protected void convert(BaseViewHolder baseViewHolder, MessageAdapterItem messageAdapterItem) {
    switch (baseViewHolder.getItemViewType()) {
      case MessageAdapterItem.SELF_TEXT:
        baseViewHolder.setText(R.id.self_message_body, messageAdapterItem.getContent().getBody());
        baseViewHolder.setText(R.id.self_message_time, messageAdapterItem.getContent().getTime());
        break;
      case MessageAdapterItem.OTHER_TEXT:
        baseViewHolder.setText(R.id.other_message_name, messageAdapterItem.getContent().getName());
        baseViewHolder.setText(R.id.other_message_body, messageAdapterItem.getContent().getBody());
        baseViewHolder.setText(R.id.other_message_time, messageAdapterItem.getContent().getTime());
        break;
    }
  }
}
