package me.ndjoe.dripmessenger.recycleradapter;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import java.util.List;
import me.ndjoe.dripmessenger.R;
import me.ndjoe.dripmessenger.common.ChatViewModel;

public class ChatListAdapter extends BaseQuickAdapter<ChatViewModel, BaseViewHolder> {
  public ChatListAdapter(List<ChatViewModel> data) {
    super(R.layout.layout_chat_list_item, data);
  }

  @Override protected void convert(BaseViewHolder baseViewHolder, ChatViewModel chatViewModel) {
    baseViewHolder.setText(R.id.chat_name, chatViewModel.getName())
        .setText(R.id.chat_message, chatViewModel.getLastMessage())
        .setText(R.id.chat_unread_count, chatViewModel.getUnreadMessageCount());
  }
}
