package me.ndjoe.dripmessenger.recycleradapter.model;

public class Contact {
  private String jid;
  private String nickname;
  private byte[] avatar;

  public Contact(String jid, String nickname, byte[] avatar) {
    this.jid = jid;
    this.nickname = nickname;
    this.avatar = avatar;
  }

  public String getJid() {
    return jid;
  }

  public String getNickname() {
    return nickname;
  }

  public byte[] getAvatar() {
    return avatar;
  }
}
