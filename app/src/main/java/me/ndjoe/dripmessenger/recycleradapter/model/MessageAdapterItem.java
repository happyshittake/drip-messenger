package me.ndjoe.dripmessenger.recycleradapter.model;

import com.chad.library.adapter.base.entity.MultiItemEntity;
import me.ndjoe.dripmessenger.common.MessageViewModel;

public class MessageAdapterItem implements MultiItemEntity {
  public static final int SELF_TEXT = 1;
  public static final int OTHER_TEXT = 2;

  private MessageViewModel content;

  public MessageAdapterItem(MessageViewModel content) {
    this.content = content;
  }

  public MessageViewModel getContent() {
    return content;
  }

  public void setContent(MessageViewModel content) {
    this.content = content;
  }

  @Override public int getItemType() {
    return content.isSelf() ? SELF_TEXT : OTHER_TEXT;
  }
}
