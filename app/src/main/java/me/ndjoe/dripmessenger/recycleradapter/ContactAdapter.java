package me.ndjoe.dripmessenger.recycleradapter;

import android.widget.ImageView;
import com.bumptech.glide.Glide;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import java.util.List;
import jp.wasabeef.glide.transformations.CropCircleTransformation;
import me.ndjoe.dripmessenger.R;
import me.ndjoe.dripmessenger.recycleradapter.model.Contact;

public class ContactAdapter extends BaseQuickAdapter<Contact, BaseViewHolder> {
  public ContactAdapter(List<Contact> data) {
    super(R.layout.layout_contact_item, data);
  }

  @Override protected void convert(BaseViewHolder baseViewHolder, Contact contact) {
    baseViewHolder.setText(R.id.contact_username, contact.getNickname());

    if (contact.getAvatar() != null) {
      Glide.with(mContext)
          .load(contact.getAvatar())
          .bitmapTransform(new CropCircleTransformation(mContext))
          .into((ImageView) baseViewHolder.getView(R.id.contact_avatar));
    } else {
      Glide.with(mContext)
          .load("")
          .placeholder(R.drawable.default_avatar)
          .into((ImageView) baseViewHolder.getView(R.id.contact_avatar));
    }
  }
}
