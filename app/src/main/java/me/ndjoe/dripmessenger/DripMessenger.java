package me.ndjoe.dripmessenger;

import android.app.Application;
import com.sjl.foreground.Foreground;
import me.ndjoe.dripmessenger.data.Logging;
import me.ndjoe.dripmessenger.injection.HasComponent;

public class DripMessenger extends Application implements HasComponent<DripMessengerComponent> {

  private DripMessengerComponent dripMessengerComponent;

  @Override public void onCreate() {
    super.onCreate();
    Foreground.init(this);
    //SqlScoutServer.create(this, getPackageName());
    Logging.ENABLED = true;
  }

  @Override public DripMessengerComponent getComponent() {
    if (dripMessengerComponent == null) {
      dripMessengerComponent = DaggerDripMessengerComponent.builder()
          .dripMessengerModule(new DripMessengerModule(this))
          .build();
    }
    return dripMessengerComponent;
  }
}
