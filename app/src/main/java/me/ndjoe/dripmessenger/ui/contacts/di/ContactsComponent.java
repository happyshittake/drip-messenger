package me.ndjoe.dripmessenger.ui.contacts.di;

import dagger.Subcomponent;
import me.ndjoe.dripmessenger.injection.FragmentScope;
import me.ndjoe.dripmessenger.ui.contacts.ContactsFragment;

@FragmentScope @Subcomponent(modules = { ContactsModule.class })
public interface ContactsComponent {
  void inject(ContactsFragment fragment);
}
