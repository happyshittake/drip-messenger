package me.ndjoe.dripmessenger.ui.chatroom.di;

import android.content.SharedPreferences;
import dagger.Module;
import dagger.Provides;
import me.ndjoe.dripmessenger.data.local.ChatRoomInteractor;
import me.ndjoe.dripmessenger.injection.ActivityScope;
import me.ndjoe.dripmessenger.ui.chatroom.ChatRoomContract;
import me.ndjoe.dripmessenger.ui.chatroom.ChatRoomPresenter;

@Module public class ChatRoomModule {

  @ActivityScope @Provides ChatRoomContract.Presenter providePresenter(
      ChatRoomInteractor chatRoomInteractor, SharedPreferences preferences) {
    return new ChatRoomPresenter(chatRoomInteractor, preferences);
  }
}
