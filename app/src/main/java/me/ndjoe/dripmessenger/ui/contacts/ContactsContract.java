package me.ndjoe.dripmessenger.ui.contacts;

import java.util.List;
import me.ndjoe.dripmessenger.common.BasePresenter;
import me.ndjoe.dripmessenger.injection.BaseView;
import me.ndjoe.dripmessenger.recycleradapter.model.Contact;

public class ContactsContract {
  public interface Presenter extends BasePresenter<View> {
    void getContacts();

    void onSelectContact(Contact contact);
  }

  public interface View extends BaseView<Presenter> {
    void showContacts(List<Contact> contacts);

    void showEmptyContacts();
  }
}
