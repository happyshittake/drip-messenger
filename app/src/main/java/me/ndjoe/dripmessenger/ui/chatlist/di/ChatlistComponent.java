package me.ndjoe.dripmessenger.ui.chatlist.di;

import dagger.Subcomponent;
import me.ndjoe.dripmessenger.injection.FragmentScope;
import me.ndjoe.dripmessenger.ui.chatlist.ChatListFragment;

@FragmentScope @Subcomponent(modules = { ChatlistModule.class })
public interface ChatlistComponent {
  void inject(ChatListFragment fragment);
}
