package me.ndjoe.dripmessenger.ui.login.di;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import dagger.Module;
import dagger.Provides;
import me.ndjoe.dripmessenger.data.XMPPConfig;
import me.ndjoe.dripmessenger.injection.ActivityScope;
import me.ndjoe.dripmessenger.ui.login.LoginActivity;
import me.ndjoe.dripmessenger.ui.login.LoginContract;
import me.ndjoe.dripmessenger.ui.login.LoginPresenter;

@Module public class LoginUIModule {
  private AppCompatActivity activity;

  public LoginUIModule(LoginActivity activity) {
    this.activity = activity;
  }

  @Provides @ActivityScope LoginContract.Presenter providePresenter(Context context,
      XMPPConfig config, SharedPreferences preferences) {
    return new LoginPresenter(context, config, preferences);
  }
}
