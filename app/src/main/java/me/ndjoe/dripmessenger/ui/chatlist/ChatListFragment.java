package me.ndjoe.dripmessenger.ui.chatlist;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.listener.OnItemClickListener;
import java.util.Collections;
import java.util.List;
import javax.inject.Inject;
import me.ndjoe.dripmessenger.DripMessenger;
import me.ndjoe.dripmessenger.R;
import me.ndjoe.dripmessenger.common.ChatViewModel;
import me.ndjoe.dripmessenger.recycleradapter.ChatListAdapter;
import me.ndjoe.dripmessenger.ui.chatlist.di.ChatlistModule;
import me.ndjoe.dripmessenger.ui.chatroom.ChatRoomActivity;

public class ChatListFragment extends Fragment implements ChatListContract.View {
  @BindView(R.id.chat_list) RecyclerView recyclerView;
  @Inject ChatListContract.Presenter presenter;
  private ChatListAdapter chatListAdapter;

  public ChatListFragment() {

  }

  @Nullable @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
      @Nullable Bundle savedInstanceState) {
    DripMessenger app = (DripMessenger) getActivity().getApplication();
    app.getComponent().plus(new ChatlistModule()).inject(this);
    View view = inflater.inflate(R.layout.fragment_chatlist, container, false);
    ButterKnife.bind(this, view);
    presenter.attachView(this);
    return view;
  }

  @Override public void onDestroy() {
    super.onDestroy();
    presenter.detachView();
  }

  @Override public void onDestroyView() {
    super.onDestroyView();
  }

  @Override public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    setUpRecycler();
  }

  private void setUpRecycler() {
    chatListAdapter = new ChatListAdapter(Collections.emptyList());
    LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
    layoutManager.setStackFromEnd(true);
    layoutManager.setReverseLayout(true);
    recyclerView.setLayoutManager(layoutManager);
    recyclerView.setAdapter(chatListAdapter);
    recyclerView.addOnItemTouchListener(new OnItemClickListener() {
      @Override public void onSimpleItemClick(BaseQuickAdapter baseQuickAdapter, View view, int i) {
        ChatViewModel chatViewModel = (ChatViewModel) baseQuickAdapter.getItem(i);
        Log.d("selected-chat", chatViewModel.toString());
        Intent intent = new Intent(getActivity(), ChatRoomActivity.class);
        intent.putExtra("chat_id", chatViewModel.getId());
        intent.putExtra("chat_type", chatViewModel.getType());
        startActivity(intent);
      }
    });
  }

  @Override public void showChatList(List<ChatViewModel> models) {
    if (chatListAdapter == null) {
      setUpRecycler();
    }

    Log.d("chats", models.toString());

    chatListAdapter.setNewData(models);
  }
}
