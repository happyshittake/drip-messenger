package me.ndjoe.dripmessenger.ui.main;

import android.content.Intent;
import javax.inject.Inject;
import me.ndjoe.dripmessenger.data.xmpp.XMPPService;
import me.ndjoe.dripmessenger.event.AddFriend;
import me.ndjoe.dripmessenger.event.AddFriendFailed;
import me.ndjoe.dripmessenger.event.AddFriendSuccess;
import me.ndjoe.dripmessenger.event.AddGroup;
import me.ndjoe.dripmessenger.event.AddGroupFailed;
import me.ndjoe.dripmessenger.event.AddGroupSuccess;
import me.ndjoe.dripmessenger.event.VCardLoaded;
import me.ndjoe.dripmessenger.event.XmppGetUserData;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

public class MainPresenter implements MainContract.Presenter {
  private MainContract.View view;

  @Inject public MainPresenter() {
  }

  @Override public void getUserData() {
    EventBus.getDefault().post(new XmppGetUserData());
  }

  @Override public void addFriend(String userJid) {
    EventBus.getDefault().post(new AddFriend(userJid));
    view.showAddingFriendDialog();
  }

  @Override public void addGroup(String topic) {
    EventBus.getDefault().post(new AddGroup(topic));
    if (view != null) {
      view.showAddingGroupDialog();
    }
  }

  @Subscribe(threadMode = ThreadMode.MAIN) public void onAddGroupSuccess(AddGroupSuccess e) {
    view.closeAddGroupDialog();
    view.openChatroom(e.getName());
  }

  @Subscribe(threadMode = ThreadMode.MAIN) public void onAddGroupFailed(AddGroupFailed e) {
    view.closeAddGroupDialog();
  }

  @Subscribe(threadMode = ThreadMode.MAIN) public void onAddFriendSuccess(AddFriendSuccess e) {
    view.closeAddFriendDialog();
  }

  @Subscribe(threadMode = ThreadMode.MAIN) public void onAddFriendFailed(AddFriendFailed e) {
    view.closeAddFriendDialog();
  }

  @Subscribe(threadMode = ThreadMode.MAIN) public void onVcardLoaded(VCardLoaded e) {
    view.loadUserData(e.getvCard());
  }

  @Override public void attachView(MainContract.View view) {
    this.view = view;
    EventBus.getDefault().register(this);
  }

  @Override public void detachView() {
    this.view = null;
    EventBus.getDefault().unregister(this);
  }
}
