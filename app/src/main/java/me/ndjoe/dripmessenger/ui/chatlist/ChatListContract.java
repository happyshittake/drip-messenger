package me.ndjoe.dripmessenger.ui.chatlist;

import java.util.List;
import me.ndjoe.dripmessenger.common.ChatViewModel;
import me.ndjoe.dripmessenger.common.BasePresenter;
import me.ndjoe.dripmessenger.injection.BaseView;

public class ChatListContract {
  public interface Presenter extends BasePresenter<View> {

  }

  public interface View extends BaseView<Presenter> {
    public void showChatList(List<ChatViewModel> models);
  }
}
