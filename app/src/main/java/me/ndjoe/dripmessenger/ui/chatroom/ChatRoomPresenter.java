package me.ndjoe.dripmessenger.ui.chatroom;

import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.util.Log;
import java.util.List;
import javax.inject.Inject;
import me.ndjoe.dripmessenger.common.ChatMapper;
import me.ndjoe.dripmessenger.common.ChatViewModel;
import me.ndjoe.dripmessenger.common.MessageMapper;
import me.ndjoe.dripmessenger.common.MessageViewModel;
import me.ndjoe.dripmessenger.data.local.ChatRoomInteractor;
import me.ndjoe.dripmessenger.event.ContactLoaded;
import me.ndjoe.dripmessenger.event.GetContact;
import me.ndjoe.dripmessenger.event.InviteUsersToGroup;
import me.ndjoe.dripmessenger.recycleradapter.model.Contact;
import me.ndjoe.dripmessenger.utils.ObserverAdapter;
import me.ndjoe.dripmessenger.utils.RxUtils;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import rx.Subscription;
import rx.subjects.BehaviorSubject;

import static solid.collectors.ToList.toList;

public class ChatRoomPresenter implements ChatRoomContract.Presenter {
  private static final int PAGE_QUANTITY = 20;

  private ChatRoomContract.View view;
  private ChatRoomInteractor chatRoomInteractor;
  private BehaviorSubject<Void> initializedSubject;
  private String chatId;
  private String currentUserId;
  private String chatType;
  private Subscription currentPageSubscription;

  @Inject
  public ChatRoomPresenter(ChatRoomInteractor chatRoomInteractor, SharedPreferences preferences) {
    this.chatRoomInteractor = chatRoomInteractor;
    this.currentUserId = preferences.getString("drip_jabber_id", null);
  }

  @Override public void setChatId(@NonNull String chatId, @NonNull String chatType) {
    Log.d("chat-id", chatId);
    Log.d("chat-type", chatType);
    this.chatId = chatId;
    this.chatType = chatType;
  }

  @Override public void promptInvite() {
    EventBus.getDefault().post(new GetContact());
  }

  @Subscribe(threadMode = ThreadMode.MAIN) public void onContactLoaded(ContactLoaded e) {
    List<String> contacts =
        solid.stream.Stream.stream(e.getContacts()).map(Contact::getJid).collect(toList());

    view.showInviteDialog(contacts.toArray(new String[contacts.size()]));
  }

  @Override public void inviteUsers(CharSequence[] contacts) {
    EventBus.getDefault().post(new InviteUsersToGroup(chatId, contacts));
  }

  @Override public void attachView(ChatRoomContract.View view) {
    this.view = view;
    EventBus.getDefault().register(this);
    initializedSubject = BehaviorSubject.create();
    Log.d("current-user-id", currentUserId);
    Log.d("chat-id-pas-attach", chatId);
    if (this.chatId != null) {
      Subscription subscription = chatRoomInteractor.initWithChatId(this.chatId)
          .map(ChatMapper::map)
          .compose(RxUtils.applySchedulers())
          .subscribe(this::initCompleted, Throwable::printStackTrace);
      RxUtils.manage(this, subscription);

      subscription = chatRoomInteractor.getNewMessages(this.chatId)
          .map(messageModels -> MessageMapper.map(messageModels, this.currentUserId))
          .compose(RxUtils.applySchedulers())
          .delaySubscription(initializedSubject)
          .doOnNext(messageViewModels -> Log.d("messages", messageViewModels.toString()))
          .subscribe(this.view::updateMessageUi, Throwable::printStackTrace);
      RxUtils.manage(this, subscription);
    }
  }

  @Override public void detachView() {
    initializedSubject = null;
    RxUtils.unsubscribe(this);
    EventBus.getDefault().unregister(this);
    view = null;
    this.chatId = null;
  }

  private void initCompleted(ChatViewModel chatViewModel) {
    view.updateChatUi(chatViewModel);
    initializedSubject.onNext(null);
  }

  @Override public void onCloseChatRoom() {
    view.closeRoom();
  }

  @Override public void onSendTextMessage(String text) {
    chatRoomInteractor.sendTextMessage(text, currentUserId, chatType)
        .compose(RxUtils.applySchedulers())
        .subscribe(ObserverAdapter.empty());
  }

  @Override public void onResendMessage(MessageViewModel model) {
    chatRoomInteractor.resendMessage(model.getModel())
        .compose(RxUtils.applySchedulers())
        .subscribe(ObserverAdapter.empty());
  }

  @Override public void onLoadMore(int page) {
    if (currentPageSubscription != null) {
      currentPageSubscription.unsubscribe();
    }

    currentPageSubscription =
        chatRoomInteractor.getMessages(chatId, page * PAGE_QUANTITY, PAGE_QUANTITY)
            .map(messageModels -> MessageMapper.map(messageModels, currentUserId))
            .compose(RxUtils.applySchedulers())
            .delaySubscription(initializedSubject)
            .subscribe(view::updateMessageUi, Throwable::printStackTrace);
    RxUtils.manage(this, currentPageSubscription);
  }
}
