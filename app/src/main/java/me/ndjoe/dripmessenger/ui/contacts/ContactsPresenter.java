package me.ndjoe.dripmessenger.ui.contacts;

import javax.inject.Inject;
import me.ndjoe.dripmessenger.event.ContactLoaded;
import me.ndjoe.dripmessenger.event.GetContact;
import me.ndjoe.dripmessenger.recycleradapter.model.Contact;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

public class ContactsPresenter implements ContactsContract.Presenter {
  private ContactsContract.View view;

  @Inject public ContactsPresenter() {

  }

  @Override public void getContacts() {
    EventBus.getDefault().post(new GetContact());
  }

  @Override public void onSelectContact(Contact contact) {

  }

  @Subscribe(threadMode = ThreadMode.MAIN) public void displayContacts(ContactLoaded e) {
    if (e.getContacts().size() > 0) {
      view.showContacts(e.getContacts());
    } else {
      view.showEmptyContacts();
    }
  }

  @Override public void attachView(ContactsContract.View view) {
    this.view = view;
    EventBus.getDefault().register(this);
  }

  @Override public void detachView() {
    this.view = null;
    EventBus.getDefault().unregister(this);
  }
}
