package me.ndjoe.dripmessenger.ui.chatroom;

import java.util.List;
import me.ndjoe.dripmessenger.common.BasePresenter;
import me.ndjoe.dripmessenger.common.ChatViewModel;
import me.ndjoe.dripmessenger.common.MessageViewModel;
import me.ndjoe.dripmessenger.injection.BaseView;

public class ChatRoomContract {
  public interface Presenter extends BasePresenter<View> {
    void onCloseChatRoom();

    void onSendTextMessage(String text);

    void onResendMessage(MessageViewModel model);

    void onLoadMore(int page);

    void setChatId(String chatId, String chatType);

    void promptInvite();

    void inviteUsers(CharSequence[] contacts);
  }

  public interface View extends BaseView<Presenter> {
    void updateMessageUi(List<MessageViewModel> newMessages);

    void updateChatUi(ChatViewModel chatViewModel);

    void closeRoom();

    void showInviteDialog(String[] contacts);
  }
}
