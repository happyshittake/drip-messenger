package me.ndjoe.dripmessenger.ui.login;

import me.ndjoe.dripmessenger.common.BasePresenter;
import me.ndjoe.dripmessenger.injection.BaseView;

public class LoginContract {
  public interface Presenter extends BasePresenter<View> {
    void login(String username, String password);

    void register(String username, String password);
  }

  public interface View extends BaseView<Presenter> {
    void resetForm();

    void submitSuccess();

    void submitError();

    void submitLoading();

    void gotoMainActivity();
  }
}
