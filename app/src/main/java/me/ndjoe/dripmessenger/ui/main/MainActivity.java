package me.ndjoe.dripmessenger.ui.main;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.afollestad.materialdialogs.MaterialDialog;
import com.bumptech.glide.Glide;
import com.mikepenz.iconics.IconicsDrawable;
import com.mikepenz.ionicons_typeface_library.Ionicons;
import com.ogaclejapan.smarttablayout.SmartTabLayout;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItem;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItemAdapter;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItems;
import javax.inject.Inject;
import jp.wasabeef.glide.transformations.CropCircleTransformation;
import me.ndjoe.dripmessenger.DripMessenger;
import me.ndjoe.dripmessenger.R;
import me.ndjoe.dripmessenger.data.XMPPConfig;
import me.ndjoe.dripmessenger.data.xmpp.XMPPService;
import me.ndjoe.dripmessenger.ui.chatroom.ChatRoomActivity;
import me.ndjoe.dripmessenger.ui.contacts.ContactsFragment;
import me.ndjoe.dripmessenger.ui.login.LoginActivity;
import me.ndjoe.dripmessenger.ui.main.di.MainModule;
import org.jivesoftware.smackx.vcardtemp.packet.VCard;

public class MainActivity extends AppCompatActivity implements MainContract.View {
  @BindView(R.id.main_toolbar) Toolbar toolbar;
  @BindView(R.id.viewPagerTab) SmartTabLayout tabLayout;
  @BindView(R.id.viewPager) ViewPager viewPager;
  @BindView(R.id.username) TextView usernameView;
  @BindView(R.id.avatar_image) ImageView avatarView;
  @Inject MainContract.Presenter presenter;
  @Inject SharedPreferences preferences;
  @Inject XMPPConfig config;

  private MaterialDialog addFriendDialog;
  private MaterialDialog addingFriendDialog;
  private MaterialDialog addGroupDialog;
  private MaterialDialog addingGroupDialog;

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    DripMessenger app = (DripMessenger) getApplication();
    app.getComponent().plus(new MainModule(this)).inject(this);
    if (preferences.getString("drip_username", null) == null) {
      preferences.edit().putString("drip_username", null).apply();
      preferences.edit().putString("drip_password", null).apply();
      preferences.edit().putString("drip_jabber_id", null).apply();
      Intent intent = new Intent(this, LoginActivity.class);
      startActivity(intent);
      finish();
    }
    if (XMPPService.getInstance() == null) {
      XMPPService.run(this, new XMPPConfig("128.199.220.162", 5222, "drip", "drip-messenger"));
    }
    setContentView(R.layout.activity_main);
    presenter.attachView(this);
    setSupportActionBar(toolbar);

    setupTabs();
  }

  @Override protected void onStart() {
    super.onStart();
    presenter.getUserData();
  }

  @Override public boolean onCreateOptionsMenu(Menu menu) {
    getMenuInflater().inflate(R.menu.menu_main, menu);
    menu.getItem(0)
        .setIcon(new IconicsDrawable(this).icon(Ionicons.Icon.ion_android_person_add)
            .color(Color.WHITE)
            .sizeDp(24));
    menu.getItem(1)
        .setIcon(new IconicsDrawable(this).icon(Ionicons.Icon.ion_android_people)
            .color(Color.WHITE)
            .sizeDp(24));
    return true;
  }

  @Override public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
      case R.id.add_friend:
        showAddFriendDialog();
        break;
      case R.id.create_group:
        showAddGroupDialog();
        break;
    }
    return super.onOptionsItemSelected(item);
  }

  @Override public void onContentChanged() {
    super.onContentChanged();
    ButterKnife.bind(this);
  }

  @Override protected void onDestroy() {
    super.onDestroy();
    presenter.detachView();
  }

  @Override protected void onStop() {
    super.onStop();
  }

  @Override public void loadUserData(VCard vCard) {
    usernameView.setText(vCard.getNickName());
    if (vCard.getAvatar() != null) {
      Glide.with(this)
          .load(vCard.getAvatar())
          .placeholder(R.drawable.default_avatar)
          .bitmapTransform(new CropCircleTransformation(this))
          .into(avatarView);
    } else {
      Glide.with(this).load("").placeholder(R.drawable.default_avatar).into(avatarView);
    }
  }

  @Override public void showAddFriendDialog() {
    if (addFriendDialog != null && addFriendDialog.isShowing()) {
      addFriendDialog.dismiss();
      addFriendDialog = null;
    }

    addFriendDialog = new MaterialDialog.Builder(this).title("Tambah teman")
        .inputType(InputType.TYPE_CLASS_TEXT)
        .input("username", null, (dialog, input) -> {
          if (!TextUtils.isEmpty(input.toString())) {
            presenter.addFriend(input.toString());
          }
          dialog.dismiss();
        })
        .build();

    addFriendDialog.show();
  }

  @Override public void showAddingGroupDialog() {
    //if (addingGroupDialog != null && addingGroupDialog.isShowing()) {
    //  addingGroupDialog.dismiss();
    //  addingGroupDialog = null;
    //}
    //
    //addingGroupDialog =
    //    new MaterialDialog.Builder(this).title("menambah group").progress(true, 0).build();
    //addingGroupDialog.show();
  }

  @Override public void showAddGroupDialog() {
    if (addGroupDialog != null && addGroupDialog.isShowing()) {
      addGroupDialog.dismiss();
      addGroupDialog = null;
    }

    addGroupDialog = new MaterialDialog.Builder(this).title("Buat group chat")
        .inputType(InputType.TYPE_CLASS_TEXT)
        .input("nama group", null, ((dialog, input) -> {
          if (!TextUtils.isEmpty(input.toString())) {
            presenter.addGroup(input.toString());
          }
          dialog.dismiss();
        }))
        .build();

    addGroupDialog.show();
  }

  @Override public void closeAddGroupDialog() {
    if (addingGroupDialog != null && addingGroupDialog.isShowing()) {
      addingGroupDialog.dismiss();
      addingGroupDialog = null;
    }
  }

  @Override public void showAddingFriendDialog() {
    if (addingFriendDialog != null && addingFriendDialog.isShowing()) {
      addingFriendDialog.dismiss();
      addingFriendDialog = null;
    }

    addingFriendDialog =
        new MaterialDialog.Builder(this).title("Menambah teman").progress(true, 0).build();

    addingFriendDialog.show();
  }

  @Override public void closeAddFriendDialog() {
    if (addingFriendDialog != null && addingFriendDialog.isShowing()) {
      addingFriendDialog.dismiss();
    }
  }

  @Override public void setupTabs() {
    tabLayout.setViewPager(viewPager);
    LayoutInflater layoutInflater = LayoutInflater.from(tabLayout.getContext());

    tabLayout.setCustomTabView((container, position, adapter) -> {
      ImageView icon = (ImageView) layoutInflater.inflate(R.layout.tab_icon, container, false);
      switch (position) {
        case 0:
          icon.setImageDrawable(new IconicsDrawable(this).icon(Ionicons.Icon.ion_android_people)
              .color(Color.WHITE)
              .sizeDp(24));
          break;
        case 1:
          icon.setImageDrawable(new IconicsDrawable(this).icon(Ionicons.Icon.ion_chatboxes)
              .color(Color.WHITE)
              .sizeDp(24));
          break;
        default:
          throw new IllegalStateException("invalid position");
      }

      return icon;
    });

    FragmentPagerItems pages = new FragmentPagerItems(this);
    int tabCount = MainRouter.tabs().length;
    for (int i = 0; i < tabCount; i++) {
      pages.add(FragmentPagerItem.of(getString(MainRouter.tabs()[i]), MainRouter.fragments()[i]));
    }

    FragmentPagerItemAdapter adapter =
        new FragmentPagerItemAdapter(getSupportFragmentManager(), pages);
    viewPager.setAdapter(adapter);
    tabLayout.setViewPager(viewPager);
  }

  @Override public void openChatroom(String chatId) {
    Intent intent = new Intent(this, ChatRoomActivity.class);
    intent.putExtra("chat_id", chatId);
    intent.putExtra("chat_type", "groupchat");
    startActivity(intent);
  }
}
