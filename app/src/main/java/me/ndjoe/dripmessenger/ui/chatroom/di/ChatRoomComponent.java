package me.ndjoe.dripmessenger.ui.chatroom.di;

import dagger.Subcomponent;
import me.ndjoe.dripmessenger.injection.ActivityScope;
import me.ndjoe.dripmessenger.ui.chatroom.ChatRoomActivity;

@ActivityScope @Subcomponent(modules = ChatRoomModule.class) public interface ChatRoomComponent {
  void inject(ChatRoomActivity chatRoomActivity);
}
