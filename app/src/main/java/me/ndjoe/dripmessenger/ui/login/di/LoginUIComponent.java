package me.ndjoe.dripmessenger.ui.login.di;

import dagger.Subcomponent;
import me.ndjoe.dripmessenger.injection.ActivityScope;
import me.ndjoe.dripmessenger.ui.login.LoginActivity;

@ActivityScope @Subcomponent(modules = { LoginUIModule.class }) public interface LoginUIComponent {
  public void inject(LoginActivity loginActivity);
}
