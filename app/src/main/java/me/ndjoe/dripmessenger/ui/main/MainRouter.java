package me.ndjoe.dripmessenger.ui.main;

import me.ndjoe.dripmessenger.R;
import me.ndjoe.dripmessenger.ui.chatlist.ChatListFragment;
import me.ndjoe.dripmessenger.ui.contacts.ContactsFragment;

public class MainRouter {

  public static int[] tabs() {
    return new int[] {
        R.string.tab_contacts, R.string.tab_chat_rooms
    };
  }

  public static Class[] fragments() {
    return new Class[] {
        ContactsFragment.class, ChatListFragment.class
    };
  }
}
