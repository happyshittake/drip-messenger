package me.ndjoe.dripmessenger.ui.login;

import android.content.Context;
import android.content.SharedPreferences;
import javax.inject.Inject;
import me.ndjoe.dripmessenger.data.XMPPConfig;
import me.ndjoe.dripmessenger.data.xmpp.XMPPService;
import me.ndjoe.dripmessenger.event.LoginXmpp;
import me.ndjoe.dripmessenger.event.RegisterUser;
import me.ndjoe.dripmessenger.event.XmppAuthenticationError;
import me.ndjoe.dripmessenger.event.XmppAuthenticationSuccess;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import rx.Subscription;

public class LoginPresenter implements LoginContract.Presenter {
  Context context;
  XMPPConfig xmppConfig;
  private LoginContract.View view;
  private Subscription authSubscription;
  private SharedPreferences preferences;

  @Inject
  public LoginPresenter(Context context, XMPPConfig xmppConfig, SharedPreferences preferences) {
    this.context = context;
    this.preferences = preferences;
    if (XMPPService.getInstance() == null) {
      XMPPService.run(context, xmppConfig);
    }
  }

  @Override public void login(String username, String password) {
    if (this.view == null) {
      return;
    }

    view.submitLoading();
    EventBus.getDefault().post(new LoginXmpp(username, password));
  }

  @Subscribe(threadMode = ThreadMode.MAIN) public void onLoginSuccess(XmppAuthenticationSuccess e) {
    view.submitSuccess();
  }

  @Subscribe(threadMode = ThreadMode.MAIN) public void onLoginError(XmppAuthenticationError e) {
    view.submitError();
  }

  @Override public void register(String username, String password) {
    view.submitLoading();
    EventBus.getDefault().post(new RegisterUser(username, password));
  }

  @Override public void attachView(LoginContract.View view) {
    this.view = view;
    EventBus.getDefault().register(this);
  }

  @Override public void detachView() {
    this.view = null;
    EventBus.getDefault().unregister(this);
    if (authSubscription != null) {
      authSubscription.unsubscribe();
    }
  }
}
