package me.ndjoe.dripmessenger.ui.main.di;

import dagger.Subcomponent;
import me.ndjoe.dripmessenger.injection.ActivityScope;
import me.ndjoe.dripmessenger.ui.main.MainActivity;

@ActivityScope @Subcomponent(modules = { MainModule.class }) public interface MainComponent {
  void inject(MainActivity activity);
}
