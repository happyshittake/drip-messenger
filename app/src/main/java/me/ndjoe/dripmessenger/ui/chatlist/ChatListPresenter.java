package me.ndjoe.dripmessenger.ui.chatlist;

import android.util.Log;
import javax.inject.Inject;
import me.ndjoe.dripmessenger.common.ChatMapper;
import me.ndjoe.dripmessenger.data.local.ChatInteractor;
import me.ndjoe.dripmessenger.injection.BasePresenter;
import me.ndjoe.dripmessenger.utils.RxUtils;
import rx.Subscription;

public class ChatListPresenter implements ChatListContract.Presenter {
  private ChatListContract.View view;
  private ChatInteractor chatInteractor;

  @Inject public ChatListPresenter(ChatInteractor chatInteractor) {
    this.chatInteractor = chatInteractor;
  }

  @Override public void attachView(ChatListContract.View view) {
    this.view = view;
    Subscription subscription = chatInteractor.getChats()
        .map(ChatMapper::map)
        .compose(RxUtils.applySchedulers())
        .subscribe(chats -> this.view.showChatList(chats), Throwable::printStackTrace);
    RxUtils.manage(this, subscription);
  }

  @Override public void detachView() {
    RxUtils.unsubscribe(this);
    this.view = null;
  }
}
