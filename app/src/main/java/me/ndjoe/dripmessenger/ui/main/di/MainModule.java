package me.ndjoe.dripmessenger.ui.main.di;

import android.support.v7.app.AppCompatActivity;
import dagger.Module;
import dagger.Provides;
import me.ndjoe.dripmessenger.injection.ActivityScope;
import me.ndjoe.dripmessenger.ui.main.MainContract;
import me.ndjoe.dripmessenger.ui.main.MainPresenter;

@Module public class MainModule {
  private AppCompatActivity appCompatActivity;

  public MainModule(AppCompatActivity appCompatActivity) {
    this.appCompatActivity = appCompatActivity;
  }

  @Provides @ActivityScope MainContract.Presenter providePresenter() {
    return new MainPresenter();
  }
}
