package me.ndjoe.dripmessenger.ui.contacts;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.listener.OnItemClickListener;
import java.util.Collections;
import java.util.List;
import javax.inject.Inject;
import me.ndjoe.dripmessenger.DripMessenger;
import me.ndjoe.dripmessenger.R;
import me.ndjoe.dripmessenger.recycleradapter.ContactAdapter;
import me.ndjoe.dripmessenger.recycleradapter.model.Contact;
import me.ndjoe.dripmessenger.ui.chatroom.ChatRoomActivity;
import me.ndjoe.dripmessenger.ui.contacts.di.ContactsModule;

public class ContactsFragment extends Fragment implements ContactsContract.View {
  @BindView(R.id.contact_list) RecyclerView contactList;
  @BindView(R.id.no_contact_view) TextView emptyView;
  @Inject ContactsContract.Presenter presenter;
  private ContactAdapter contactAdapter;

  public ContactsFragment() {

  }

  @Nullable @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
      @Nullable Bundle savedInstanceState) {
    DripMessenger app = (DripMessenger) getActivity().getApplication();
    app.getComponent().plus(new ContactsModule()).inject(this);
    View view = inflater.inflate(R.layout.fragment_contact, container, false);
    ButterKnife.bind(this, view);
    presenter.attachView(this);
    return view;
  }

  @Override public void onDestroyView() {
    super.onDestroyView();
    presenter.detachView();
  }

  @Override public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    contactAdapter = new ContactAdapter(Collections.emptyList());
    contactList.setLayoutManager(new LinearLayoutManager(getContext()));
    contactList.setAdapter(contactAdapter);
    contactList.addOnItemTouchListener(new OnItemClickListener() {
      @Override public void onSimpleItemClick(BaseQuickAdapter baseQuickAdapter, View view, int i) {
        Contact contact = (Contact) baseQuickAdapter.getItem(i);
        Intent intent = new Intent(getActivity(), ChatRoomActivity.class);
        Log.d("selected-contact", contact.toString());
        intent.putExtra("chat_id", contact.getJid());
        intent.putExtra("chat_type", "chat");
        startActivity(intent);
      }
    });
    presenter.getContacts();
  }

  @Override public void showContacts(List<Contact> contacts) {
    if (contactAdapter == null) {
      contactAdapter = new ContactAdapter(Collections.emptyList());
    }
    contactList.setVisibility(View.VISIBLE);
    emptyView.setVisibility(View.GONE);
    contactAdapter.setNewData(contacts);
  }

  @Override public void showEmptyContacts() {
    emptyView.setVisibility(View.VISIBLE);
    contactList.setVisibility(View.GONE);
  }
}
