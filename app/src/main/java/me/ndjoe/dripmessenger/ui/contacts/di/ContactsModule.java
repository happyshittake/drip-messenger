package me.ndjoe.dripmessenger.ui.contacts.di;

import dagger.Module;
import dagger.Provides;
import me.ndjoe.dripmessenger.injection.FragmentScope;
import me.ndjoe.dripmessenger.ui.contacts.ContactsContract;
import me.ndjoe.dripmessenger.ui.contacts.ContactsPresenter;

@Module public class ContactsModule {
  @FragmentScope @Provides ContactsContract.Presenter providePresenter() {
    return new ContactsPresenter();
  }
}
