package me.ndjoe.dripmessenger.ui.main;

import me.ndjoe.dripmessenger.common.BasePresenter;
import me.ndjoe.dripmessenger.injection.BaseView;
import org.jivesoftware.smackx.vcardtemp.packet.VCard;

public class MainContract {
  public interface Presenter extends BasePresenter<View> {
    void getUserData();

    void addFriend(String userJid);

    void addGroup(String topic);
  }

  public interface View extends BaseView<Presenter> {
    void loadUserData(VCard vCard);

    void showAddFriendDialog();

    void showAddingGroupDialog();

    void showAddGroupDialog();

    void closeAddGroupDialog();

    void showAddingFriendDialog();

    void closeAddFriendDialog();

    void setupTabs();

    void openChatroom(String chatId);
  }
}
