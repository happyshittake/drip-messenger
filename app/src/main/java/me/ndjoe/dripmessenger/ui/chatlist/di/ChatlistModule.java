package me.ndjoe.dripmessenger.ui.chatlist.di;

import dagger.Module;
import dagger.Provides;
import me.ndjoe.dripmessenger.data.local.ChatInteractor;
import me.ndjoe.dripmessenger.injection.FragmentScope;
import me.ndjoe.dripmessenger.ui.chatlist.ChatListContract;
import me.ndjoe.dripmessenger.ui.chatlist.ChatListPresenter;

@Module public class ChatlistModule {
  @FragmentScope @Provides ChatListContract.Presenter providePresenter(
      ChatInteractor chatInteractor) {
    return new ChatListPresenter(chatInteractor);
  }
}
