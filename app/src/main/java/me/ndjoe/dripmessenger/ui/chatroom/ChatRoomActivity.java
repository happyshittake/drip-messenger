package me.ndjoe.dripmessenger.ui.chatroom;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.afollestad.materialdialogs.MaterialDialog;
import com.mikepenz.iconics.IconicsDrawable;
import com.mikepenz.iconics.view.IconicsImageView;
import com.mikepenz.ionicons_typeface_library.Ionicons;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.inject.Inject;
import me.ndjoe.dripmessenger.DripMessenger;
import me.ndjoe.dripmessenger.R;
import me.ndjoe.dripmessenger.common.ChatViewModel;
import me.ndjoe.dripmessenger.common.MessageViewModel;
import me.ndjoe.dripmessenger.recycleradapter.MessageListAdapter;
import me.ndjoe.dripmessenger.recycleradapter.model.MessageAdapterItem;
import me.ndjoe.dripmessenger.ui.chatroom.di.ChatRoomModule;
import solid.stream.Stream;

import static solid.collectors.ToList.toList;

public class ChatRoomActivity extends AppCompatActivity implements ChatRoomContract.View {
  @Inject ChatRoomContract.Presenter presenter;

  @BindView(R.id.room_messages) RecyclerView messageList;
  @BindView(R.id.room_chat_text_field) EditText textField;
  @BindView(R.id.room_chat_send_button) IconicsImageView sendButton;
  @BindView(R.id.room_name) TextView roomName;
  @BindView(R.id.chat_room_toolbar) Toolbar toolbar;
  private String chatType;
  private MessageListAdapter messageListAdapter;
  private MaterialDialog contactChooserDialog;

  @Override protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    DripMessenger app = (DripMessenger) getApplication();
    app.getComponent().plus(new ChatRoomModule()).inject(this);
    setContentView(R.layout.activity_chat_room);
    setSupportActionBar(toolbar);
    String s = getIntent().getStringExtra("chat_id");
    chatType = getIntent().getStringExtra("chat_type");
    presenter.setChatId(s, chatType);
    presenter.attachView(this);
    setUpRecycler();
  }

  @Override public boolean onCreateOptionsMenu(Menu menu) {
    if (Objects.equals(chatType, "groupchat")) {
      getMenuInflater().inflate(R.menu.menu_group_chat, menu);
      menu.getItem(0)
          .setIcon(new IconicsDrawable(this).icon(Ionicons.Icon.ion_plus_circled)
              .color(Color.WHITE)
              .sizeDp(24));
    }
    return super.onCreateOptionsMenu(menu);
  }

  @Override public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
      case R.id.invite_group:
        presenter.promptInvite();
        break;
    }
    return super.onOptionsItemSelected(item);
  }

  @Override public void onContentChanged() {
    super.onContentChanged();
    ButterKnife.bind(this);
  }

  @OnClick(R.id.room_chat_send_button) public void onSendTextMessage(View v) {
    String text = textField.getText().toString();
    if (TextUtils.isEmpty(text)) {
      return;
    }

    presenter.onSendTextMessage(text);
    textField.setText("");
  }

  private void setUpRecycler() {
    messageListAdapter = new MessageListAdapter(new ArrayList<>());
    LinearLayoutManager layoutManager = new LinearLayoutManager(this);
    layoutManager.setStackFromEnd(true);
    layoutManager.setReverseLayout(true);
    messageList.setLayoutManager(layoutManager);
    messageList.setAdapter(messageListAdapter);
    presenter.onLoadMore(0);
  }

  @Override protected void onDestroy() {
    super.onDestroy();
    presenter.detachView();
  }

  @Override public void updateMessageUi(List<MessageViewModel> newMessages) {
    messageListAdapter.addData(0,
        Stream.stream(newMessages).map(MessageAdapterItem::new).collect(toList()));
    messageList.smoothScrollToPosition(0);
  }

  @Override public void updateChatUi(ChatViewModel chatViewModel) {
    Log.d("chat-view-model", chatViewModel.toString());
    roomName.setText(chatViewModel.getName());
  }

  @Override public void closeRoom() {
    finish();
  }

  @Override public void showInviteDialog(String[] contacts) {
    if (contactChooserDialog != null && contactChooserDialog.isShowing()) {
      contactChooserDialog.dismiss();
      contactChooserDialog = null;
    }

    contactChooserDialog = new MaterialDialog.Builder(this).title("pilih kontak")
        .items(contacts)
        .itemsCallbackMultiChoice(null, (dialog, which, text) -> {
          presenter.inviteUsers(text);
          return false;
        })
        .positiveText("Invite")
        .build();

    contactChooserDialog.show();
  }
}
