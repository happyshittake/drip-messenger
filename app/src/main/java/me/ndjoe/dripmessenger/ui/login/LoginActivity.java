package me.ndjoe.dripmessenger.ui.login;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import com.afollestad.materialdialogs.MaterialDialog;
import javax.inject.Inject;
import me.ndjoe.dripmessenger.DripMessenger;
import me.ndjoe.dripmessenger.R;
import me.ndjoe.dripmessenger.ui.login.di.LoginUIModule;
import me.ndjoe.dripmessenger.ui.main.MainActivity;

public class LoginActivity extends AppCompatActivity implements LoginContract.View {

  @BindView(R.id.login_button) Button loginButton;
  @BindView(R.id.register_button) Button registerButton;
  @BindView(R.id.username_field) EditText usernameField;
  @BindView(R.id.password_field) EditText passwordField;
  @BindView(R.id.login_error_view) TextView errorView;
  @Inject LoginContract.Presenter presenter;
  private Unbinder unbinder;
  private MaterialDialog dialog;

  @Override protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    DripMessenger app = (DripMessenger) getApplication();
    app.getComponent().plus(new LoginUIModule(this)).inject(this);
    setContentView(R.layout.login_activity);
  }

  @Override public void onContentChanged() {
    super.onContentChanged();
    unbinder = ButterKnife.bind(this);
    presenter.attachView(this);
  }

  @Override protected void onStart() {
    super.onStart();
  }

  @Override protected void onStop() {
    super.onStop();
    presenter.detachView();
    unbinder.unbind();
  }

  @Override public void resetForm() {
    usernameField.setText("");
    passwordField.setText("");
    errorView.setVisibility(View.GONE);
  }

  @Override public void submitSuccess() {
    if (dialog != null && dialog.isShowing()) {
      dialog.dismiss();
    }
    gotoMainActivity();
  }

  @Override public void submitError() {
    errorView.setVisibility(View.VISIBLE);
  }

  @Override public void submitLoading() {
    if (dialog != null && dialog.isShowing()) {
      dialog.dismiss();
    }
    dialog = new MaterialDialog.Builder(this).content("Logging in...").progress(true, 0).build();
    dialog.show();
  }

  @Override public void gotoMainActivity() {
    Intent intent = new Intent(this, MainActivity.class);
    startActivity(intent);
    finish();
  }

  @OnClick(R.id.login_button) void onLoginButton() {
    String username = usernameField.getText().toString();
    String password = passwordField.getText().toString();
    presenter.login(username, password);
  }

  @OnClick(R.id.register_button) void onRegisterButton() {
    String username = usernameField.getText().toString();
    String password = passwordField.getText().toString();
    presenter.register(username, password);
  }
}
