package me.ndjoe.dripmessenger.injection;

public interface HasComponent<C> {
  C getComponent();
}
