package me.ndjoe.dripmessenger.injection;

public interface BasePresenter<V> {
  void subscribe(V view);

  void unsubscribe();
}
